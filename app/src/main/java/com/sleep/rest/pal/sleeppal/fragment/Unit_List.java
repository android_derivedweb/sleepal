package com.sleep.rest.pal.sleeppal.fragment;

import static com.sleep.rest.pal.sleeppal.MainScreen.mBack;
import static com.sleep.rest.pal.sleeppal.MainScreen.mPerson;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.rest.pal.sleeppal.LoginScreen;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.adapter.AdapterUnits2;
import com.sleep.rest.pal.sleeppal.model.UnitListModels;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Unit_List extends Fragment {
    private AdapterUnits2 mAdapterUnits;
    private RecyclerView mRecyclerView;
    // Store instance variablesversionDetails
    private ArrayList<UnitListModels> mUnitListModels = new ArrayList<>();
    private RequestQueue requestQueue;
    private UserSession mUserSession;
    private TextView mTotalCount;
    private TextView mDate;


    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    // Inflate the view for the fragment based on layout XML
    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todays, container, false);
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue
        mUserSession = new UserSession(getContext());


        mPerson.setVisibility(View.VISIBLE);
        mBack.setVisibility(View.INVISIBLE);
        mDate = view.findViewById(R.id.mDate);
        mTotalCount = view.findViewById(R.id.mTotalCount);
        mRecyclerView = view.findViewById(R.id.mRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapterUnits = new AdapterUnits2(getActivity(), mUnitListModels, new AdapterUnits2.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


            }
        });
        mRecyclerView.setAdapter(mAdapterUnits);
        UnitListRequest();
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        mDate.setText("Date : "+formattedDate);

        return view;
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();

    }

    private void UnitListRequest() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL+"get-total-units",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        mUnitListModels.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("response",jsonObject.toString());
                            if(jsonObject.getString("ResponseCode").equals("200")){

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0 ; i<jsonArray.length() ; i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    UnitListModels listModels = new UnitListModels();

                                    listModels.setRoomName(object.getString("room_name"));
                                    listModels.setDescription(object.getString("description"));
                                  //  listModels.setChecked_CNT(object.getString("checked_cnt"));
                                  //  listModels.setNights_CNT(object.getString("nights_cnt"));
                                   // listModels.setEarning_CNT(object.getString("earning_cnt"));
                                  //  listModels.setExpense_CNT(object.getString("expense_cnt"));
                                  //  listModels.setExpense_CNT("");
                                  //  listModels.setTotal_CNT(object.getString("total_cnt"));
                                   // listModels.setRoom_ID(object.getString("room_id"));
                                    mUnitListModels.add(listModels);
                                }

                                mTotalCount.setText(""+mUnitListModels.size());
                                mAdapterUnits.notifyDataSetChanged();
                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }



                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if(error.networkResponse.statusCode == 401){
                            mUserSession.logout();
                            startActivity(new Intent(getActivity(), LoginScreen.class));
                            getActivity().finishAffinity();

                        }
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                //  params.put("email", mEmail);
                // params.put("first_name", mName);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }






}