package com.sleep.rest.pal.sleeppal.fragment;

import static com.sleep.rest.pal.sleeppal.MainScreen.mBack;
import static com.sleep.rest.pal.sleeppal.MainScreen.mPerson;

import android.annotation.SuppressLint;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.rest.pal.sleeppal.LoginScreen;
import com.sleep.rest.pal.sleeppal.MainScreen;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.Utils;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Dashboard extends Fragment {
    private RequestQueue requestQueue;
    private UserSession mUserSession;
    private TextView total_earning, total_expence, mTotalMonth, total_units, todays_availability, todays_checkin, current_staying, todays_checkout, upcoming_guests;
    // Store instance variablesversionDetails

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    // Inflate the view for the fragment based on layout XML
    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue
        mUserSession = new UserSession(getContext());


        if (Build.VERSION.SDK_INT >= 33) {
            if (Utils.check_notification_permission(requireActivity())) {
//                firstBtnClick();
            }
        }

        total_earning = view.findViewById(R.id.total_earning);
        mTotalMonth = view.findViewById(R.id.mTotalMonth);
        total_expence = view.findViewById(R.id.total_expence);
        total_units = view.findViewById(R.id.total_units);
        todays_availability = view.findViewById(R.id.todays_availability);
        todays_checkin = view.findViewById(R.id.todays_checkin);
        current_staying = view.findViewById(R.id.current_staying);
        todays_checkout = view.findViewById(R.id.todays_checkout);
        upcoming_guests = view.findViewById(R.id.upcoming_guests);

        DateFormat dateFormat = new SimpleDateFormat("MMM, yyyy");
        Date date = new Date();
        Log.d("Month", dateFormat.format(date));

        TextView mCurrentMonth = (TextView) view.findViewById(R.id.mCurrentMonth);
        mCurrentMonth.setText("BALANCE " + dateFormat.format(date));
        mPerson.setVisibility(View.VISIBLE);
        mBack.setVisibility(View.INVISIBLE);
        Log.e("qwqw", mUserSession.getAPIToken());
        MainScreen.mTitle.setText("Dashboard");
        view.findViewById(R.id.mUnitList).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainScreen.mTitle.setText("Unit List");
                replaceFragment(R.id.fragment_layout, new Unit_List(), "Fragment", "Fragment");
            }
        });

        view.findViewById(R.id.mTotalEarning).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainScreen.mTitle.setText("Account");
                replaceFragment(R.id.fragment_layout, new Account(), "Fragment", "Fragment");
            }
        });

        view.findViewById(R.id.mTodayAvailibility).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainScreen.mTitle.setText("Availibility");
                replaceFragment(R.id.fragment_layout, new Availability_Calendar(), "Fragment", "Fragment");
            }
        });

        view.findViewById(R.id.mCHeckin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainScreen.mTitle.setText("Check In");
                replaceFragment(R.id.fragment_layout, new Check_In(), "Fragment", "Fragment");
            }
        });
        view.findViewById(R.id.mcurrentStaying).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainScreen.mTitle.setText("Staying");
                replaceFragment(R.id.fragment_layout, new Staying(), "Fragment", "Fragment");
            }
        });
        view.findViewById(R.id.mCHeckOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainScreen.mTitle.setText("Check Out");
                replaceFragment(R.id.fragment_layout, new Check_Out(), "Fragment", "Fragment");
            }
        });
        view.findViewById(R.id.mUpcoming).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainScreen.mTitle.setText("Upcoming");
                replaceFragment(R.id.fragment_layout, new Upcoming(), "Fragment", "Fragment");
            }
        });

        DashBoardRequest();

        return view;
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();

    }


    private void DashBoardRequest() {

        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-home-screen-details",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                //Log.e("dfdsf",jsonObject.toString());
                                //Log.e("dfdsf", mUserSession.getAPIToken());

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    total_earning.setText("Total Earning " + object.getString("total_earning"));
                                    total_expence.setText("Total Expense " + object.getString("total_expense"));
                                    mTotalMonth.setText(object.getString("total_balance"));
                                    total_units.setText(object.getString("total_units"));
                                    todays_availability.setText(object.getString("todays_availability"));
                                    todays_checkin.setText(object.getString("todays_checkin"));
                                    current_staying.setText(object.getString("current_staying"));
                                    todays_checkout.setText(object.getString("todays_checkout"));
                                    upcoming_guests.setText(object.getString("upcoming_guests"));
                                }
                            } else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

//                        if (error.networkResponse.statusCode == 401) {
//                            mUserSession.logout();
//                            startActivity(new Intent(getActivity(), LoginScreen.class));
//                            getActivity().finishAffinity();
//                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError){
//                            Toast.makeText(getContext(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();
                        }
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                //  params.put("email", mEmail);
                // params.put("first_name", mName);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                //  params.put("Authorization", "Bearer tJb09fK1J5jrP92dmcPvSGNQXnQKjIrmThRZuyUueaw7lSY7om6EkmV9ik8J");
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


}