package com.sleep.rest.pal.sleeppal;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.Utils;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class Edit_My_Profile extends AppCompatActivity {

    private EditText mName, mPhone, mAddress, mAccountNumber, mBankAccName, mBankName;
    private ImageView image_dialog;
    private TextView mEmail, mUpdatebtn, mJoinDate, mEndDate, mReferral;
    private RequestQueue requestQueue;
    private UserSession mUserSession;
    private Bitmap profile_bitmap;
    private String strBankName = "", strBankAccName = "", strAccNum = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_profile);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.blue));

        requestQueue = Volley.newRequestQueue(Edit_My_Profile.this);//Creating the RequestQueue
        mUserSession = new UserSession(Edit_My_Profile.this);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        image_dialog = findViewById(R.id.image_dialog);
        mName = findViewById(R.id.mName);
        mEmail = findViewById(R.id.mEmail);
        mPhone = findViewById(R.id.mPhone);
        mAddress = findViewById(R.id.mAddress);
        mBankName = findViewById(R.id.mBankName);
        mBankAccName = findViewById(R.id.mBankAccName);
        mAccountNumber = findViewById(R.id.mAccountNumber);
        mJoinDate = findViewById(R.id.mJoinDate);
        mEndDate = findViewById(R.id.mEndDate);
        mReferral = findViewById(R.id.mReferral);
        mUpdatebtn = findViewById(R.id.edit_profile);


        mUpdatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mName.getText().toString().trim().isEmpty()) {
                    Toast.makeText(Edit_My_Profile.this, "Please Enter Your Name!", Toast.LENGTH_SHORT).show();
                } else {
                    boolean changed = !mBankName.getText().toString().equals(strBankName) ||
                            !mBankAccName.getText().toString().equals(strBankAccName) ||
                            !mAccountNumber.getText().toString().equals(strAccNum);

                    if (changed) {
                        dialog_password();
                    } else {
                        UpdateRequest(mEmail.getText().toString().trim(), mName.getText().toString());
                    }
                }
            }
        });

        findViewById(R.id.image_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.checkCamStoragePer(Edit_My_Profile.this)) {
                    selectImage();
                }
            }
        });


        getProfile();

        Glide.with(Edit_My_Profile.this).load(mUserSession.getUserProfile()).placeholder(ContextCompat.getDrawable(this, R.drawable.person_icon))
                .circleCrop().into(image_dialog);

    }

    private void getProfile() {

        final KProgressHUD progressDialog = KProgressHUD.create(Edit_My_Profile.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL
                + "get-profile?owner_id=" + mUserSession.getUserID(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("getProfile", jsonObject.toString());

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject data = jsonObject.getJSONObject("data");

                                mName.setText(data.getString("first_name"));
                                mEmail.setText(data.getString("email"));
                                mPhone.setText(data.getString("phone"));
                                mAddress.setText(data.getString("address"));

                                if (!data.getString("bank_name").equals("null")) {
                                    strBankName = data.getString("bank_name");
                                    mBankName.setText(strBankName);
                                }
                                if (!data.getString("account_name").equals("null")) {
                                    strBankAccName = data.getString("account_name");
                                    mBankAccName.setText(strBankAccName);
                                }
                                if (!data.getString("bank_account_number").equals("null")) {
                                    strAccNum = data.getString("bank_account_number");
                                    mAccountNumber.setText(strAccNum);
                                }

                                if (!data.getString("owner_join_date").equals("null")) {
                                    mJoinDate.setText(data.getString("owner_join_date"));
                                }
                                if (!data.getString("contract_end_date").equals("null")) {
                                    mEndDate.setText(data.getString("contract_end_date"));
                                }
                                if (!data.getString("referral").equals("null")) {
                                    mReferral.setText(data.getString("referral"));
                                }

                            } else {
                                Toast.makeText(Edit_My_Profile.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(Edit_My_Profile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(Edit_My_Profile.this, LoginScreen.class));
                            finishAffinity();
                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(Edit_My_Profile.this, R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Edit_My_Profile.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Edit_My_Profile.this, R.string.bad_netword_connection, Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("owner_id", mUserSession.getUserID());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }

    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;

    private void selectImage() {

        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.gallary), getString(R.string.cancel)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(Edit_My_Profile.this);
        builder.setTitle(R.string.select_option);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.take_photo))) {
                    dialog.dismiss();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, PICK_IMAGE_CAMERA);
                } else if (options[item].equals(getResources().getString(R.string.gallary))) {
                    dialog.dismiss();
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();

                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                File folder = new File(getExternalFilesDir(null) + "/" + "Sleepal");

                if (!folder.exists()) {
                    folder.mkdirs();
                }
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                File destination = new File(Environment.getExternalStorageDirectory() + "/" +
                        "Sleepal", "SP_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                profile_bitmap = Utils.setOrientationBitmap(Edit_My_Profile.this, bitmap);
                Glide.with(Edit_My_Profile.this).asBitmap().load(bitmap).circleCrop().into(image_dialog);

                String imgPath = destination.getAbsolutePath();
                // final_path = new File(Environment.getExternalStorageDirectory() + "/" +
                //         "Human Perform", "IMG_" + timeStamp + ".jpg");
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (requestCode == PICK_IMAGE_GALLERY) {

            if (data != null) {
                try {
                    Uri selectedImageUri = data.getData();
                    String selectedImagePath = getRealPathFromURI(selectedImageUri);
                    Log.e("selectedImagePath", selectedImagePath);

                    InputStream imageStream = null;
                    try {
                        imageStream = getContentResolver().openInputStream(selectedImageUri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    final Bitmap imagebitmap = BitmapFactory.decodeStream(imageStream);

                    String path = getPath(selectedImageUri);
                    Matrix matrix = new Matrix();
                    ExifInterface exif = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        try {
                            exif = new ExifInterface(path);
                            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.postRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.postRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.postRotate(270);
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    Bitmap bitmap = Bitmap.createBitmap(imagebitmap, 0, 0, imagebitmap.getWidth(), imagebitmap.getHeight(), matrix, true);


                    String imgPath = getRealPathFromURI(selectedImageUri);
                    profile_bitmap = bitmap;
                    Glide.with(Edit_My_Profile.this).asBitmap().load(bitmap).circleCrop().into(image_dialog);

                    File destination = new File(imgPath.toString());
                    //  txt_injury.setText(imgPath);
                    // final_path = new File(imgPath.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    }

    public String getPath(Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void UpdateRequest(String mEmail, String mName) {

        Log.e("getInfo", mEmail + "--" + mName + "--" + mUserSession.getAPIToken() + "--" + mUserSession.getUserID());
        final KProgressHUD progressDialog = KProgressHUD.create(Edit_My_Profile.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "update-profile",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("getUpdateProfile", jsonObject.toString());

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                mUserSession.setUserName(mName);
                                mUserSession.setUserProfile(jsonObject.getJSONObject("data").getString("profile_pic"));
                                Toast.makeText(Edit_My_Profile.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                strBankName = mBankName.getText().toString().trim();
                                strBankAccName = mBankAccName.getText().toString().trim();
                                strAccNum = mAccountNumber.getText().toString().trim();

                            } else {
                                Toast.makeText(Edit_My_Profile.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            Toast.makeText(Edit_My_Profile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        Log.e("sadsadas", error.getMessage());
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(Edit_My_Profile.this, LoginScreen.class));
                            finishAffinity();
                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(Edit_My_Profile.this, R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Edit_My_Profile.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Edit_My_Profile.this, R.string.bad_netword_connection, Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("owner_id", mUserSession.getUserID());
                params.put("email", mEmail);
                params.put("first_name", mName);
                params.put("phone", mPhone.getText().toString().trim());
                params.put("address", mAddress.getText().toString().trim());
                params.put("bank_name", mBankName.getText().toString().trim());
                params.put("bank_account_name", mBankAccName.getText().toString().trim());
                params.put("account_number", mAccountNumber.getText().toString().trim());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                if (profile_bitmap != null) {
                    long imagename = System.currentTimeMillis();
                    params.put("file", new DataPart(imagename + ".png", getFileDataFromDrawable(profile_bitmap)));
                }


                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }

    private void verifyPassword(String password, Dialog dialog) {

        final KProgressHUD progressDialog = KProgressHUD.create(Edit_My_Profile.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "update-profile",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("getVerifyPassword", jsonObject.toString());

                            if (jsonObject.getString("ResponseCode").equals("200")) {
                                dialog.dismiss();

                                UpdateRequest(mEmail.getText().toString().trim(), mName.getText().toString());

                            } else {
                                Toast.makeText(Edit_My_Profile.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(Edit_My_Profile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        Log.e("sadsadas", error.getMessage());
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(Edit_My_Profile.this, LoginScreen.class));
                            finishAffinity();
                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(Edit_My_Profile.this, R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Edit_My_Profile.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Edit_My_Profile.this, R.string.bad_netword_connection, Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("verify_password", password);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                if (profile_bitmap != null) {
                    long imagename = System.currentTimeMillis();
                    params.put("file", new DataPart(imagename + ".png", getFileDataFromDrawable(profile_bitmap)));
                }

                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }

    private void dialog_password() {
        Dialog dialog = new Dialog(Edit_My_Profile.this);
        dialog.setContentView(R.layout.dialog_password);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        EditText mPassword = dialog.findViewById(R.id.mPassword);
        TextView submit = dialog.findViewById(R.id.submit);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyPassword(mPassword.getText().toString().trim(), dialog);
            }
        });

        dialog.show();
    }

}