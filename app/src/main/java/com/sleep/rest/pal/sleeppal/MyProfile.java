package com.sleep.rest.pal.sleeppal;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.pal.sleeppal.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MyProfile extends AppCompatActivity {

    private TextView mEmail, mName, mPhone, mAddress, mAccountNumber, mBankAccName, mBankName, mJoinDate, mEndDate, mReferral;
    private ImageView image_dialog;
    private UserSession mUserSession;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        mUserSession = new UserSession(MyProfile.this);
        requestQueue = Volley.newRequestQueue(MyProfile.this);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.blue));


        findViewById(R.id.edit_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyProfile.this, Edit_My_Profile.class));
            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mName = findViewById(R.id.mName);
        mEmail = findViewById(R.id.mEmail);
        mPhone = findViewById(R.id.mPhone);
        mAddress = findViewById(R.id.mAddress);
        mBankName = findViewById(R.id.mBankName);
        mBankAccName = findViewById(R.id.mBankAccName);
        mAccountNumber = findViewById(R.id.mAccountNumber);
        mJoinDate = findViewById(R.id.mJoinDate);
        mEndDate = findViewById(R.id.mEndDate);
        mReferral = findViewById(R.id.mReferral);
        image_dialog = findViewById(R.id.image_dialog);

    }

    @Override
    protected void onResume() {
        super.onResume();

        Glide.with(MyProfile.this).load(mUserSession.getUserProfile())
                .placeholder(ContextCompat.getDrawable(this, R.drawable.person_icon)).circleCrop().into(image_dialog);
        getProfile();
    }

    private void getProfile() {

        final KProgressHUD progressDialog = KProgressHUD.create(MyProfile.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL
                + "get-profile?owner_id=" + mUserSession.getUserID(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("getProfile", jsonObject.toString());

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject data = jsonObject.getJSONObject("data");

                                mName.setText(data.getString("first_name"));
                                mEmail.setText(data.getString("email"));
                                mPhone.setText(data.getString("phone"));
                                mAddress.setText(data.getString("address"));

                                if (!data.getString("bank_name").equals("null")) {
                                    mBankName.setText(data.getString("bank_name"));
                                }
                                if (!data.getString("account_name").equals("null")) {
                                    mBankAccName.setText(data.getString("account_name"));
                                }
                                if (!data.getString("bank_account_number").equals("null")) {
                                    mAccountNumber.setText(data.getString("bank_account_number"));
                                }

                                if (!data.getString("owner_join_date").equals("null")) {
                                    mJoinDate.setText(data.getString("owner_join_date"));
                                }
                                if (!data.getString("contract_end_date").equals("null")) {
                                    mEndDate.setText(data.getString("contract_end_date"));
                                }
                                if (!data.getString("referral").equals("null")) {
                                    mReferral.setText(data.getString("referral"));
                                }


                            } else {
                                Toast.makeText(MyProfile.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(MyProfile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(MyProfile.this, LoginScreen.class));
                            finishAffinity();
                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(MyProfile.this, R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(MyProfile.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(MyProfile.this, R.string.bad_netword_connection, Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("owner_id", mUserSession.getUserID());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);
    }


}