package com.sleep.rest.pal.sleeppal.fragment;

import static com.sleep.rest.pal.sleeppal.MainScreen.mBack;
import static com.sleep.rest.pal.sleeppal.MainScreen.mPerson;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.rest.pal.sleeppal.LoginScreen;
import com.sleep.rest.pal.sleeppal.MainScreen;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.adapter.AdapterUnits;
import com.sleep.rest.pal.sleeppal.model.UnitListModels;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Staying extends Fragment {
    // Store instance variablesversionDetails
    // Store instance variables based on arguments passed

    private AdapterUnits mAdapterUnits;
    private RecyclerView mRecyclerView;
    // Store instance variablesversionDetails
    private ArrayList<UnitListModels> mUnitListModels = new ArrayList<>();
    private RequestQueue requestQueue;
    private UserSession mUserSession;
    private TextView mTotalCount;
    private TextView mDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    // Inflate the view for the fragment based on layout XML
    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_staying, container, false);

        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue
        mUserSession = new UserSession(getContext());

        MainScreen.mTitle.setText("Staying");
        mPerson.setVisibility(View.VISIBLE);
        mBack.setVisibility(View.INVISIBLE);
        mDate = view.findViewById(R.id.mDate);
        mTotalCount = view.findViewById(R.id.mTotalCount);
        mRecyclerView = view.findViewById(R.id.mRecyclerView);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapterUnits = new AdapterUnits(getActivity(), mUnitListModels, new AdapterUnits.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                MainScreen.mTitle.setText("Staying Details");
                Staying_details fragobj = new Staying_details();
                Bundle bundle = new Bundle();
                bundle.putString("OrderID", mUnitListModels.get(item).getOrderID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragment_layout, fragobj, "Fragment", "Fragment");
            }
        });
        mRecyclerView.setAdapter(mAdapterUnits);


        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        mDate.setText("Date : " + formattedDate);

        UnitListRequest();

        return view;
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }

    private String getDate(String mDate) {
        DateTimeFormatter inputFormatter = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
            LocalDate date = LocalDate.parse(mDate, inputFormatter);
            String appDate = outputFormatter.format(date);

            return appDate;
        } else {
            return mDate;
        }
    }


    private void UnitListRequest() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-current-staying",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        mUnitListModels.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray jsonArray1 = jsonObject.getJSONArray("data");
                                JSONObject object1 = jsonArray1.getJSONObject(0);
                                JSONArray jsonArray = object1.getJSONArray("staying");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    UnitListModels listModels = new UnitListModels();
                                    listModels.setRoomName(object.getString("room_name"));
                                    listModels.setExpense_CNT(object.getString("name"));
                                    listModels.setOrderID(object.getString("order_id"));
                                    //  listModels.setOrderID(object.getString("order_id"));
                                    //  listModels.setNights_No(object.getString("nights"));
                                    //  listModels.setPrice("IDR "+object.getString("price"));
                                    //  listModels.setTotalPrice("IDR "+object.getString("total"));
                                    //  listModels.setCheckIN("Check In "+getDate(object.getString("checkin")));
                                    //  listModels.setCheckOUT("Check Out " +getDate(object.getString("checkout")));
                                    //  listModels.setReferral("Referral "+object.getString("referral"));
                                    listModels.setLogo(object.getString("ota_logo_url"));

                                    mUnitListModels.add(listModels);
                                }

                                mTotalCount.setText("" + mUnitListModels.size());
                                mAdapterUnits.notifyDataSetChanged();

                            } else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Log.e("Exception", e.getMessage());
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(getActivity(), LoginScreen.class));
                            getActivity().finishAffinity();
                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


}