package com.sleep.rest.pal.sleeppal.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.model.AccountListModel;

import java.util.ArrayList;

public class AdapterAccountExpense extends RecyclerView.Adapter<AdapterAccountExpense.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<AccountListModel> bookingModelsArrayList;

    public AdapterAccountExpense(Context mContext, ArrayList<AccountListModel> bookingModelsArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.bookingModelsArrayList = bookingModelsArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_account_expense, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        holder.mTitle.setText(bookingModelsArrayList.get(position).getTitle());
        holder.mCHeckin.setText(bookingModelsArrayList.get(position).getCheckin());
        holder.mAddOnAmount.setText(bookingModelsArrayList.get(position).getAddon_amount() + " ");
        holder.mValue.setText(bookingModelsArrayList.get(position).getValue() + " ");

    }

    @Override
    public int getItemCount() {
        return bookingModelsArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView mTitle, mCHeckin, mNight_cnt, mValue, mExpense, mTotal, mAddOnAmount;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            mTitle = itemView.findViewById(R.id.mTitle);
            mCHeckin = itemView.findViewById(R.id.mCHeckin);
            mValue = itemView.findViewById(R.id.mValue);
            mAddOnAmount = itemView.findViewById(R.id.mAddOnAmount);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}