package com.sleep.rest.pal.sleeppal.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UserSession {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;


    // public static String BASEURL = "https://beforesubmit.com/sleeprest/api/";

    public static String BASEURL = "https://sleeppal.sleeprest.id/api/";
//    public static String BASEURL = "https://cybertwo.net/api/";

//    public static String BASEURL = "https://sleeppal.sleeprest.id/localdb/api/";

    private static final String PREF_NAME = "SleepRest_1.0";

    private static final String IS_LOGIN = "IsLogin";
    private static final String KEY_USERID = "KEY_USERID";
    private static final String KEY_USERNAME = "KEY_USERNAME";
    private static final String KEY_EMAIL = "KEY_EMAIL";
    private static final String KEY_PHONE = "KEY_PHONE";
    private static final String KEY_PROFILEPIC = "KEY_PROFILEPIC";
    private static final String KEY_APITOKEN = "KEY_APITOKEN";
    private static final String KEY_DEVICE_TOKEN = "KEY_DEVICE_TOKEN";
    private static final String KEY_IS_TEST_MODE = "KEY_IS_TEST_MODE";


    public UserSession(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public String getCurrentDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());
        return currentDateandTime;
    }

    public String getUserID() {
        return sharedPreferences.getString(KEY_USERID, "");
    }

    public String getUserName() {
        return sharedPreferences.getString(KEY_USERNAME, "");
    }

    public String getUserProfile() {
        return sharedPreferences.getString(KEY_PROFILEPIC, "");
    }

    public void setUserProfile(String Email) {
        editor.putString(KEY_PROFILEPIC, Email);
        editor.commit();
    }

    public boolean isTestMode() {
        return sharedPreferences.getBoolean(KEY_IS_TEST_MODE, false);
    }

    public void setKeyIsTestMode(boolean b) {
        editor.putBoolean(KEY_IS_TEST_MODE, b);
        editor.commit();
    }

    public String getEmail() {
        return sharedPreferences.getString(KEY_EMAIL, "");
    }

    public void setEmail(String Email) {
        editor.putString(KEY_EMAIL, Email);
        editor.commit();
    }

    public String getKeyDeviceToken() {
        return sharedPreferences.getString(KEY_DEVICE_TOKEN, "");
    }

    public void setKeyDeviceToken(String Email) {
        editor.putString(KEY_DEVICE_TOKEN, Email);
        editor.commit();
    }

    public void setUserName(String Email) {
        editor.putString(KEY_USERNAME, Email);
        editor.commit();
    }

    public String getAPIToken() {
        return sharedPreferences.getString(KEY_APITOKEN, "");
    }

    public void createLoginSession(String user_id,
                                   String user_name,
                                   String email,
                                   String phone,
                                   String profile_pic,
                                   String api_token) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        // Storing name in pref
        editor.putString(KEY_USERID, user_id);
        editor.putString(KEY_USERNAME, user_name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_PROFILEPIC, profile_pic);
        editor.putString(KEY_PHONE, phone);
        editor.putString(KEY_APITOKEN, api_token);
        editor.commit();
    }


    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public boolean logout() {
        return sharedPreferences.edit().clear().commit();

    }

}
