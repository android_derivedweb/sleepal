package com.sleep.rest.pal.sleeppal.model;

public class CityModel {

    String Cityname;
    String CityId;

    public CityModel() {
    }

    public CityModel(String cityId, String cityname) {
        CityId = cityId;
        Cityname = cityname;
    }

    public String getCityname() {
        return Cityname;
    }

    public void setCityname(String cityname) {
        Cityname = cityname;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

}
