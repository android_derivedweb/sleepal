package com.sleep.rest.pal.sleeppal.model;

public class AccountModels {

    private String room_name;
    private String checked_cnt;
    private String nights_cnt;
    private String earning_cnt;
    private String expense_cnt;
    private String total_cnt;
    private String room_id;

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getChecked_cnt() {
        return checked_cnt;
    }

    public void setChecked_cnt(String checked_cnt) {
        this.checked_cnt = checked_cnt;
    }

    public String getNights_cnt() {
        return nights_cnt;
    }

    public void setNights_cnt(String nights_cnt) {
        this.nights_cnt = nights_cnt;
    }

    public String getEarning_cnt() {
        return earning_cnt;
    }

    public void setEarning_cnt(String earning_cnt) {
        this.earning_cnt = earning_cnt;
    }

    public String getExpense_cnt() {
        return expense_cnt;
    }

    public void setExpense_cnt(String expense_cnt) {
        this.expense_cnt = expense_cnt;
    }

    public String getTotal_cnt() {
        return total_cnt;
    }

    public void setTotal_cnt(String total_cnt) {
        this.total_cnt = total_cnt;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }
}
