package com.sleep.rest.pal.sleeppal.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.MainScreen;

import java.util.Map;

public class MyFirebaseMessaging extends FirebaseMessagingService {
    public static int NOTIFICATION_ID = 1;

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);

        Log.e("newTokenFromNoti", token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e("remoteMessageGetData", remoteMessage.getData() + "");
        sendNotification(remoteMessage.getData());

    }

    private void sendNotification(Map<String, String> data) {
        int num = ++NOTIFICATION_ID;

        Bundle msg = new Bundle();
        for (String key : data.keySet()) {
            Log.e("key", key + " " + data.get(key));
            msg.putString(key, data.get(key));
        }

        Intent backIntent;
        backIntent = new Intent(getApplicationContext(), MainScreen.class);
        backIntent.putExtra("id", msg.getString("id"));
        backIntent.putExtra("type", msg.getString("type"));
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        Log.e("getInDetailNotification", msg.getString("id") + "--" + msg.getString("type"));



        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = getString(R.string.app_name) + "channel_01";// The id of the channel.
        CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.

        NotificationCompat.Builder mBuilder;
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }
        mBuilder = new NotificationCompat.Builder(
                this, CHANNEL_ID);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel(mChannel);
        }

        //Content intent : intent on notification click
        int contentIntentFlags = PendingIntent.FLAG_CANCEL_CURRENT;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            contentIntentFlags |= PendingIntent.FLAG_IMMUTABLE;

        PendingIntent contentIntent = PendingIntent.getActivity(this, num, backIntent, contentIntentFlags);

        mBuilder.setSmallIcon(R.mipmap.ic_launcher)
//                .setContent(collapseView)
//                .setCustomBigContentView(expandedView)
                .setContentTitle(msg.getString("title"))
                .setContentText(msg.getString("body"))
                .setAutoCancel(true)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg.getString("body")))
                .setChannelId(CHANNEL_ID)
                .setContentIntent(contentIntent);

       // mNotificationManager.notify(num, mBuilder.build());
        mNotificationManager.notify(num, mBuilder.build());
    }


}
