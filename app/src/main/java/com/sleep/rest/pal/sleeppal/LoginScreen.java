package com.sleep.rest.pal.sleeppal;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.sleep.pal.sleeppal.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginScreen extends AppCompatActivity {


    private TextView mForgotPassword;
    private TextView mLogin;
    private EditText mEmail;
    private EditText mPassword;
    private RequestQueue requestQueue;
    private UserSession mUserSession;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        requestQueue = Volley.newRequestQueue(LoginScreen.this);//Creating the RequestQueue
        mUserSession = new UserSession(LoginScreen.this);

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.e("device_token", "Fetching FCM registration token failed", task.getException());
                            mUserSession.setKeyDeviceToken("Test");
                            return;
                        }
                        // Get new FCM registration token
                        String token = task.getResult();
                        mUserSession.setKeyDeviceToken(token);
                        // Log and toast
                        Log.e("device_token", token);
                    }
                });


        mForgotPassword = findViewById(R.id.forgot);
        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginScreen.this, ForgotScreen.class));
            }
        });
        mLogin = findViewById(R.id.login);

        mEmail = findViewById(R.id.m_email);
        mPassword = findViewById(R.id.m_password);

        mUserSession.setKeyIsTestMode(true);

        if (mUserSession.isTestMode()){
            mEmail.setText("apartments@sleeprest.id");
            mPassword.setText("martabak77");
        }

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mEmail.getText().toString().trim().isEmpty()) {
                    Toast.makeText(LoginScreen.this, "Please Enter Your Email!!!", Toast.LENGTH_SHORT).show();
//                }else if(!mEmail.getText().toString().trim().matches(emailPattern)){
//                    Toast.makeText(LoginScreen.this, "Please Enter Valid Your Email!!!", Toast.LENGTH_SHORT).show();
                } else if (mPassword.getText().toString().isEmpty()) {
                    Toast.makeText(LoginScreen.this, "Please Enter Your Password!!!", Toast.LENGTH_SHORT).show();
                } else {
                    LoginRequest(mEmail.getText().toString().trim(), mPassword.getText().toString());
                }
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //  mLogin.setText(mUserSession.getCurrentDateTime());
            }
        }, 1000);
    }

    private void LoginRequest(String mEmail, String mPassword) {
        final KProgressHUD progressDialog = KProgressHUD.create(LoginScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "login",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {
                                JSONObject mJsonData = jsonObject.getJSONObject("data");

                                Log.e("mUserSession", mJsonData.toString());
                                mUserSession.createLoginSession(
                                        mJsonData.getString("owner_id"),
                                        mJsonData.getString("first_name"),
                                        mJsonData.getString("email"),
                                        mJsonData.getString("phone"),
                                        mJsonData.getString("profile_pic"),
                                        mJsonData.getString("api_token")
                                );
                                startActivity(new Intent(LoginScreen.this, MainScreen.class));
                                finish();

                            } else {
                                Toast.makeText(LoginScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            Toast.makeText(LoginScreen.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(LoginScreen.this, R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(LoginScreen.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(LoginScreen.this, R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", mEmail);
                params.put("password", mPassword);
                params.put("device_type", "android");
                params.put("device_token", mUserSession.getKeyDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


}