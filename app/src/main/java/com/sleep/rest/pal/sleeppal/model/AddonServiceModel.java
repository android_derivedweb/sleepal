package com.sleep.rest.pal.sleeppal.model;

public class AddonServiceModel {

    private String addon_service_id;
    private String addon_service_name;
    private String amount;
    private String transaction_type;

    public String getAddon_service_id() {
        return addon_service_id;
    }

    public void setAddon_service_id(String addon_service_id) {
        this.addon_service_id = addon_service_id;
    }

    public String getAddon_service_name() {
        return addon_service_name;
    }

    public void setAddon_service_name(String addon_service_name) {
        this.addon_service_name = addon_service_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

}
