package com.sleep.rest.pal.sleeppal.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.model.CheckINModels;

import java.util.ArrayList;

public class AdapterCheckIN extends RecyclerView.Adapter<AdapterCheckIN.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<CheckINModels> bookingModelsArrayList;

    public AdapterCheckIN(Context mContext, ArrayList<CheckINModels> bookingModelsArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.bookingModelsArrayList = bookingModelsArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_checkin, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        holder.mRoomName.setText(bookingModelsArrayList.get(position).getRoomName());
        holder.mGuestName.setText(bookingModelsArrayList.get(position).getGuestName());
        holder.mTotalNight.setText(bookingModelsArrayList.get(position).getNights_No());
        holder.mTotalPrice.setText(bookingModelsArrayList.get(position).getTotalPrice());
        holder.mPrice.setText(bookingModelsArrayList.get(position).getPrice());
        holder.mCHeckin.setText(bookingModelsArrayList.get(position).getCheckIN());
        holder.mCHeckOut.setText(bookingModelsArrayList.get(position).getCheckOUT());
        holder.mReferral.setText(bookingModelsArrayList.get(position).getReferral());

        Glide.with(mContext).load(bookingModelsArrayList.get(position).getLogo()).into(holder.bookingIcon);

    }

    @Override
    public int getItemCount() {
        return bookingModelsArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView mRoomName, mGuestName, mTotalNight, mTotalPrice, mPrice, mCHeckin, mCHeckOut, mReferral;
        ImageView bookingIcon;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            mRoomName = itemView.findViewById(R.id.mRoomName);
            mGuestName = itemView.findViewById(R.id.mGuestName);
            mTotalNight = itemView.findViewById(R.id.mTotalNight);
            mPrice = itemView.findViewById(R.id.mPrice);
            mTotalPrice = itemView.findViewById(R.id.mTotalPrice);
            mCHeckin = itemView.findViewById(R.id.mCHeckin);
            mCHeckOut = itemView.findViewById(R.id.mCHeckOut);
            mReferral = itemView.findViewById(R.id.mReferral);
            bookingIcon = itemView.findViewById(R.id.bookingIcon);
        }
    }


    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}