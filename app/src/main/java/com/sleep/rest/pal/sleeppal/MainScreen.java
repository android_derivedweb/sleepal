package com.sleep.rest.pal.sleeppal;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.sleep.rest.pal.sleeppal.fragment.Account;
import com.sleep.rest.pal.sleeppal.fragment.AllBookings;
import com.sleep.rest.pal.sleeppal.fragment.Availability_Calendar;
import com.sleep.rest.pal.sleeppal.fragment.Check_In;
import com.sleep.rest.pal.sleeppal.fragment.Check_Out;
import com.sleep.rest.pal.sleeppal.fragment.Dashboard;
import com.sleep.rest.pal.sleeppal.fragment.LiveFeed;
import com.sleep.rest.pal.sleeppal.fragment.Staying;
import com.sleep.rest.pal.sleeppal.fragment.Todays_Availiblity;
import com.sleep.rest.pal.sleeppal.fragment.Upcoming;
import com.sleep.rest.pal.sleeppal.utils.UserSession;

import java.util.Objects;

import com.sleep.pal.sleeppal.R;

public class MainScreen extends AppCompatActivity {

    private LinearLayout mDashboardHome, mCheckin, mCheckout, mProfit;
    private ImageView mDrawerMenu;
    public static ImageView mPerson;
    public static ImageView mBack;
    public ImageView profile_image1;
    public static TextView mTitle;
    public TextView name1;
    private UserSession mUserSession;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        mUserSession = new UserSession(MainScreen.this);

        Log.e("apiToken", mUserSession.getAPIToken());

        mDashboardHome = findViewById(R.id.mDashboardHome);
        name1 = findViewById(R.id.name1);
        mCheckin = findViewById(R.id.checkin);
        mCheckout = findViewById(R.id.checkout);
        mProfit = findViewById(R.id.profit);
        profile_image1 = findViewById(R.id.profile_image1);
        mTitle = findViewById(R.id.mTitle);
        mDrawerMenu = findViewById(R.id.mDrawermenu);
        mPerson = findViewById(R.id.mPerson);
        mBack = findViewById(R.id.mBack);

        mPerson.setVisibility(View.VISIBLE);
        mBack.setVisibility(View.GONE);


        mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_black));
        mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
        mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
        mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
        replaceFragment(R.id.fragment_layout, new Dashboard(), "Fragment");

        mDashboardHome.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_black));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new Dashboard(), "Fragment");
                mTitle.setText("Dashboard");
            }
        });

        mCheckin.setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_black));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new Check_In(), "Fragment");
                mTitle.setText(R.string.today_s_check_in);
            }
        });

        mCheckout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_black));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new Check_Out(), "Fragment");
                mTitle.setText(R.string.today_s_check_out);
            }
        });

        mProfit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_black));
                replaceFragment(R.id.fragment_layout, new Account(), "Fragment");
                mTitle.setText("Accounting");
            }
        });
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        mDrawerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        findViewById(R.id.mDashboard).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_black));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new Dashboard(), "Fragment");
                mTitle.setText("Dashboard");
                drawer.close();
            }
        });

        findViewById(R.id.mAvailabilityCalendar).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new Availability_Calendar(), "Fragment");
                mTitle.setText("Availability Calendar");
                drawer.close();
            }
        });

        findViewById(R.id.mTodayAvailibility).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_black));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new Todays_Availiblity(), "Fragment");
                mTitle.setText("Today’s Availability");
                drawer.close();
            }
        });

        findViewById(R.id.mTodayCheckIn).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_black));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new Check_In(), "Fragment");
                mTitle.setText("Today’s Check-In");
                drawer.close();
            }
        });

        findViewById(R.id.mTodayCheckOut).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_black));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new Check_Out(), "Fragment");
                mTitle.setText("Today’s Check-Out");
                drawer.close();
            }
        });

        findViewById(R.id.mUpcoming).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new Upcoming(), "Fragment");
                mTitle.setText("Upcoming Guest");
                drawer.close();
            }
        });

        findViewById(R.id.mAllBookings).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new AllBookings(), "Fragment");
                mTitle.setText("All Bookings");
                drawer.close();
            }
        });

        findViewById(R.id.mLiveFeed).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new LiveFeed(), "Fragment");
                mTitle.setText("Live Feed");
                drawer.close();
            }
        });

        findViewById(R.id.mStaying).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                replaceFragment(R.id.fragment_layout, new Staying(), "Fragment");
                mTitle.setText("Current Staying");
                drawer.close();


            }
        });
        findViewById(R.id.mAccounting).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
            @Override
            public void onClick(View view) {
                mDashboardHome.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckin.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mCheckout.setBackground(getResources().getDrawable(R.drawable.broder_white_0));
                mProfit.setBackground(getResources().getDrawable(R.drawable.broder_black));
                replaceFragment(R.id.fragment_layout, new Account(), "Fragment");
                mTitle.setText("Accounting");
                drawer.close();
            }
        });

        findViewById(R.id.mLogOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUserSession.logout();
                startActivity(new Intent(MainScreen.this, SplashScreen.class));
                finish();
            }
        });

        findViewById(R.id.profile_image1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainScreen.this, MyProfile.class));
            }
        });

        findViewById(R.id.mChangePass).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainScreen.this, ChangePasswordScreen.class));
            }
        });

        findViewById(R.id.mBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
            // Execute code when returning to YourPreviousFragment
            if (currentFragment instanceof AllBookings) {
                ((AllBookings) currentFragment).onFragmentResume();
            } else if (currentFragment instanceof LiveFeed) {
                ((LiveFeed) currentFragment).onFragmentResume();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        name1.setText(mUserSession.getUserName());
        Log.e("sdsdfsf", mUserSession.getUserProfile());
        Glide.with(Objects.requireNonNull(MainScreen.this)).load(mUserSession.getUserProfile()).circleCrop().placeholder(getResources().getDrawable(R.drawable.person_icon)).into(profile_image1);
        Glide.with(Objects.requireNonNull(MainScreen.this)).load(mUserSession.getUserProfile()).circleCrop().placeholder(getResources().getDrawable(R.drawable.person_icon)).into(mPerson);

    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                //.addToBackStack(fragmentTag)
                .commit();
    }

}