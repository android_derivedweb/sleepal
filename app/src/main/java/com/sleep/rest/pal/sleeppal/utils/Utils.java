package com.sleep.rest.pal.sleeppal.utils;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.sleep.pal.sleeppal.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utils {

    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    public static boolean check_notification_permission(Activity context) {

        String[] PERMISSIONS = new String[0];
        PERMISSIONS = new String[]{
                Manifest.permission.POST_NOTIFICATIONS
        };

        if (!hasPermissions(context, PERMISSIONS)) {
            context.requestPermissions(PERMISSIONS, 200);
        } else {
            return true;
        }
        return false;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    public static Bitmap setOrientationBitmap(Context context, Bitmap bitmap) {
        Matrix rotationMatrix = new Matrix();
        if (bitmap.getWidth() >= bitmap.getHeight()) {
            rotationMatrix.setRotate(90);
        } else {
            rotationMatrix.setRotate(0);
        }
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), rotationMatrix, true);
        return bitmap;
    }

    public static void openUrlExternal(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage("com.android.chrome");
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            // Chrome browser presumably not installed so allow user to choose instead
            intent.setPackage(null);
            context.startActivity(intent);
        }
    }


    public static boolean checkCamStoragePer(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
//            boolean hasPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_MEDIA_VISUAL_USER_SELECTED)
//                    == PackageManager.PERMISSION_GRANTED;
            boolean hasCamera = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
            if (hasCamera) {
                return true;
            } else {
                return checkPermission(activity, Manifest.permission.CAMERA);
            }

        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.TIRAMISU) {
            return checkPermission(activity, Manifest.permission.CAMERA);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return checkPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
        } else
            return checkPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
    }


    public static boolean checkPermission(Activity activity, String... permissions) {
        boolean allPermitted = false;
        for (String permission : permissions) {
            allPermitted = (ContextCompat.checkSelfPermission(activity, permission)
                    == PackageManager.PERMISSION_GRANTED);
            if (!allPermitted)
                break;
        }
        if (allPermitted)
            return true;
        ActivityCompat.requestPermissions(activity, permissions,
                220);
        return false;
    }


    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static void showDatePickerDialog(Context context, TextView dateTV, String startDate) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, (view, selectedYear, selectedMonth, selectedDay) -> {
                    String formattedDate = String.format("%04d-%02d-%02d",
                            selectedYear, selectedMonth + 1, selectedDay);
                    dateTV.setText(formattedDate);
                },
                year, month, day
        );

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormat.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!startDate.isEmpty()) {
            datePickerDialog.getDatePicker().setMinDate(date.getTime());
        }
        datePickerDialog.show();
    }


}
