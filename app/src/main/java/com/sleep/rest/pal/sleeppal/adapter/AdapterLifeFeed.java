package com.sleep.rest.pal.sleeppal.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.model.LiveFeedModel;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

public class AdapterLifeFeed extends RecyclerView.Adapter<AdapterLifeFeed.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<LiveFeedModel> liveFeedArrayList;

    public AdapterLifeFeed(Context mContext, ArrayList<LiveFeedModel> liveFeedArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.liveFeedArrayList = liveFeedArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_live_feed, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        holder.status.setText(liveFeedArrayList.get(position).getStatus());
        holder.referral.setText(liveFeedArrayList.get(position).getReferral());
        holder.checkIn.setText(getDate(liveFeedArrayList.get(position).getCheckin(), false));
        holder.nameNights.setText(liveFeedArrayList.get(position).getName() + " ● " + liveFeedArrayList.get(position).getNights() + " nights");

        holder.bookingDate.setText(getTimeAgo(liveFeedArrayList.get(position).getCreated_at()));
        holder.createdAt.setText(getDate(liveFeedArrayList.get(position).getCreated_at(), true));
        holder.noGuest.setText(liveFeedArrayList.get(position).getGuests() + " guests");
        holder.total.setText(liveFeedArrayList.get(position).getTotal() + " IDR");

    }

    @Override
    public int getItemCount() {
        return liveFeedArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView status, referral, checkIn, nameNights, bookingDate, createdAt, noGuest, total;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            status = itemView.findViewById(R.id.status);
            referral = itemView.findViewById(R.id.referral);
            checkIn = itemView.findViewById(R.id.checkIn);
            nameNights = itemView.findViewById(R.id.nameNights);
            bookingDate = itemView.findViewById(R.id.bookingDate);
            createdAt = itemView.findViewById(R.id.createdAt);
            noGuest = itemView.findViewById(R.id.noGuest);
            total = itemView.findViewById(R.id.total);
        }
    }


    public interface OnItemClickListener {
        void onItemClick(int item);
    }

    private String getDate(String mDate, boolean includeTime) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            DateTimeFormatter outputFormatter;

            if (includeTime) {
                // Format: "Mon 03 Mar, 2025 02:24 PM"
                outputFormatter = DateTimeFormatter.ofPattern("EEE dd MMM, yyyy hh:mm a", Locale.ENGLISH);
            } else {
                // Format: "Sun 02 Mar, 2025"
                outputFormatter = DateTimeFormatter.ofPattern("EEE dd MMM, yyyy", Locale.ENGLISH);
            }
            LocalDateTime dateTime = LocalDateTime.parse(mDate, inputFormatter);
            return outputFormatter.format(dateTime);
        } else {
            return mDate;
        }
    }

    public static String getTimeAgo(String pastDate) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            LocalDateTime pastDateTime = LocalDateTime.parse(pastDate, formatter);
            LocalDateTime currentDateTime = LocalDateTime.now();

            Duration duration = Duration.between(pastDateTime, currentDateTime);
            long seconds = duration.getSeconds();
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (minutes < 1) {
                return "Just now";
            } else if (minutes == 1) {
                return "1 minute ago";
            } else if (minutes < 60) {
                return minutes + " minutes ago";
            } else if (hours == 1) {
                return "1 hour ago";
            } else if (hours < 24) {
                return hours + " hours ago";
            } else if (days == 1) {
                return "Yesterday";
            } else {
                return days + " days ago";
            }
        } else {
            return "Time not supported";
        }
    }


}