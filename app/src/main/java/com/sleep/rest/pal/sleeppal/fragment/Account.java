package com.sleep.rest.pal.sleeppal.fragment;

import static com.sleep.rest.pal.sleeppal.MainScreen.mBack;
import static com.sleep.rest.pal.sleeppal.MainScreen.mPerson;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.rest.pal.sleeppal.LoginScreen;
import com.sleep.rest.pal.sleeppal.MainScreen;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.adapter.AdapterAccount;
import com.sleep.rest.pal.sleeppal.model.AccountModels;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Account extends Fragment {
    // Store instance variablesversionDetails
    private TextView mTotalMonth, total_earning, total_expense, mCurrentMonth;
    private static TextView fromDate, toDate;
    private static String fromDateStr, toDateStr;
    // Store instance variables based on arguments passed
    private ArrayList<AccountModels> mUnitListModels = new ArrayList<>();
    private LinearLayout mDateFilter;
    private RecyclerView mRecyclerView;
    private AdapterAccount mAdapterUnits;
    private RequestQueue requestQueue;
    private UserSession mUserSession;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    // Inflate the view for the fragment based on layout XML
    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);


        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue
        mUserSession = new UserSession(getContext());
        fromDate = view.findViewById(R.id.fromDate);
        toDate = view.findViewById(R.id.toDate);
        mTotalMonth = view.findViewById(R.id.mTotalMonth);
        total_earning = view.findViewById(R.id.total_earning);
        total_expense = view.findViewById(R.id.total_expence);
        mDateFilter = view.findViewById(R.id.mDateFilter);
        mCurrentMonth = (TextView) view.findViewById(R.id.mCurrentMonth);


        mPerson.setVisibility(View.VISIBLE);
        mBack.setVisibility(View.INVISIBLE);
        mDateFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (fromDate.getText().equals("Select date")) {
                    Toast.makeText(getActivity(), "Please Select Start Date!!!", Toast.LENGTH_SHORT).show();
                } else if (toDate.getText().equals("Select date")) {
                    Toast.makeText(getActivity(), "Please Select To  Date!!!", Toast.LENGTH_SHORT).show();
                } else {
                    AccountRequest(fromDate.getText().toString(), toDate.getText().toString());
                }
            }
        });
        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDate.setText("Select date");
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getChildFragmentManager(), "datePicker");
            }
        });
        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fromDate.getText().equals("Select date")) {
                    DialogFragment newFragment = new DatePickerFragment2();
                    newFragment.show(getChildFragmentManager(), "datePicker2");
                } else {
                    Toast.makeText(getActivity(), "Please Select Start Date!!!", Toast.LENGTH_SHORT).show();
                }

            }
        });
        MainScreen.mTitle.setText("Account");

        mRecyclerView = view.findViewById(R.id.mRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapterUnits = new AdapterAccount(getActivity(), mUnitListModels, new AdapterAccount.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                MainScreen.mTitle.setText("Account Details");
                AccountDetails fragobj = new AccountDetails();
                Bundle bundle = new Bundle();
                bundle.putString("OrderID", mUnitListModels.get(item).getRoom_id());
                bundle.putString("fromDate", fromDate.getText().toString());
                bundle.putString("toDate", toDate.getText().toString());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragment_layout, fragobj, "Fragment", "Fragment");

            }
        });
        mRecyclerView.setAdapter(mAdapterUnits);
        AccountRequest();
        //   replaceFragment(R.id.fragment_layout, new AccountDetails(), "Fragment","Fragment");
        // AccountRequest();

        return view;
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            //dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String dateString = dateFormat.format(calendar.getTime());

            fromDate.setText(dateString);


            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            fromDateStr = dateFormat1.format(calendar.getTime());

        }
    }

    public static class DatePickerFragment2 extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = null;
            try {
                date = dateFormat.parse(fromDate.getText().toString());
                Log.e("acac", date.toString() + "--");
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("acac", "error" + "--");

            }

            dialog.getDatePicker().setMinDate(date.getTime());
            //  dialog.getDatePicker().setMaxDate(c.getTimeInMillis());

            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String dateString = dateFormat.format(calendar.getTime());

            toDate.setText(dateString);


            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            toDateStr = dateFormat1.format(calendar.getTime());
        }
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();

    }


    private void AccountRequest() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "accounting",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        mUnitListModels.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("ResponseAccounting", jsonObject.toString());

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    AccountModels listModels = new AccountModels();

                                    listModels.setRoom_id(object.getString("room_id"));
                                    listModels.setEarning_cnt(object.getString("earning_cnt"));
                                    listModels.setChecked_cnt(object.getString("checked_cnt"));
                                    listModels.setExpense_cnt(object.getString("expense_cnt"));
                                    listModels.setNights_cnt(object.getString("nights_cnt"));
                                    listModels.setRoom_name(object.getString("room_name"));
                                    listModels.setTotal_cnt(object.getString("total_cnt"));

                                    mUnitListModels.add(listModels);
                                }
                                mAdapterUnits.notifyDataSetChanged();


                                JSONObject balanceObj = jsonObject.getJSONObject("balance_summary");
                                mCurrentMonth.setText(balanceObj.getString("title"));
                                mTotalMonth.setText(balanceObj.getString("balance"));
                                total_earning.setText(balanceObj.getString("total_earning"));
                                total_expense.setText(balanceObj.getString("total_expense"));

                            } else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Log.e("Exception", e.getMessage());
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(getActivity(), LoginScreen.class));
                            getActivity().finishAffinity();
                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                //  params.put("email", mEmail);
                // params.put("first_name", mName);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }

    private void AccountRequest(String start_date, String to_date) {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "accounting?from_date=" + start_date + "&to_date=" + to_date,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        mUnitListModels.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("ResponseAccountingSbmt", jsonObject.toString());

                            if (jsonObject.getString("ResponseCode").equals("200")) {
                                Log.e("eweqwe", jsonObject.toString());
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject object = jsonArray.getJSONObject(i);
                                    AccountModels listModels = new AccountModels();
                                    listModels.setRoom_id(object.getString("room_id"));
                                    listModels.setEarning_cnt(object.getString("earning_cnt"));
                                    listModels.setChecked_cnt(object.getString("checked_cnt"));
                                    listModels.setExpense_cnt(object.getString("expense_cnt"));
                                    listModels.setNights_cnt(object.getString("nights_cnt"));
                                    listModels.setRoom_name(object.getString("room_name"));
                                    listModels.setTotal_cnt(object.getString("total_cnt"));

                                    mUnitListModels.add(listModels);
                                }

                                mAdapterUnits.notifyDataSetChanged();

                                JSONObject balanceObj = jsonObject.getJSONObject("balance_summary");
                                mCurrentMonth.setText(balanceObj.getString("title"));
                                mTotalMonth.setText(balanceObj.getString("balance"));
                                total_earning.setText(balanceObj.getString("total_earning"));
                                total_expense.setText(balanceObj.getString("total_expense"));


                            } else {

                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            Log.e("Exception", e.getMessage());
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                //  params.put("email", mEmail);
                // params.put("first_name", mName);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


}