package com.sleep.rest.pal.sleeppal;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.sleep.pal.sleeppal.R;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotScreen extends AppCompatActivity {


    private TextView mVerifyOTP,mSend;
    private RelativeLayout mVerifyOTPDialog;
    private RequestQueue requestQueue;
    private UserSession mUserSession;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private EditText mEmail;
    private EditText mOTPtxt;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        requestQueue = Volley.newRequestQueue(ForgotScreen.this);
        //Creating the RequestQueue
        mUserSession = new UserSession(ForgotScreen.this);


        mEmail = findViewById(R.id.m_email);
        mVerifyOTPDialog = findViewById(R.id.otp_dialog);
        mVerifyOTP = findViewById(R.id.done);
        mSend = findViewById(R.id.send);
        mOTPtxt = findViewById(R.id.otp_edit);

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mEmail.getText().toString().trim().isEmpty()){
                    Toast.makeText(ForgotScreen.this, "Please Enter Your Email!!!", Toast.LENGTH_SHORT).show();
                }else if(!mEmail.getText().toString().trim().matches(emailPattern)){
                    Toast.makeText(ForgotScreen.this, "Please Enter Valid Your Email!!!", Toast.LENGTH_SHORT).show();
                }else {
                    ForgotRequest(mEmail.getText().toString().trim());
                }

            }
        });

        mVerifyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOTPtxt.getText().toString().length() < 4){
                    Toast.makeText(ForgotScreen.this, "Please Enter 4 Digit OTP!!!", Toast.LENGTH_SHORT).show();
                }else {
                    startActivity(new Intent(ForgotScreen.this,NewPasswordScreen.class)
                            .putExtra("mOTP",mOTPtxt.getText().toString())
                            .putExtra("Email",mEmail.getText().toString())
                    );
                }

            }
        });




    }

    private void ForgotRequest(String mEmail) {
        final KProgressHUD progressDialog = KProgressHUD.create(ForgotScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL+"forgot-password",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if(jsonObject.getString("ResponseCode").equals("200")){
                                Toast.makeText(ForgotScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                mVerifyOTPDialog.setVisibility(View.VISIBLE);
                            }else {
                                Toast.makeText(ForgotScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(ForgotScreen.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(ForgotScreen.this, R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(ForgotScreen.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(ForgotScreen.this, R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", mEmail);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
              //  params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



}