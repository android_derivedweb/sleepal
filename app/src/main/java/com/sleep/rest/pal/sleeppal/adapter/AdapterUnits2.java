package com.sleep.rest.pal.sleeppal.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.model.UnitListModels;

import java.util.ArrayList;

public class AdapterUnits2 extends RecyclerView.Adapter<AdapterUnits2.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;

    private ArrayList<UnitListModels> bookingModelsArrayList;


    public AdapterUnits2(Context mContext, ArrayList<UnitListModels> bookingModelsArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.bookingModelsArrayList = bookingModelsArrayList;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_unit, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
        holder.room_name.setText(bookingModelsArrayList.get(position).getRoomName());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.price.setText(Html.fromHtml(bookingModelsArrayList.get(position).getDescription(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            holder.price.setText(Html.fromHtml(bookingModelsArrayList.get(position).getDescription()));

        }
        holder.mTitle.setText("Name");




    }

    @Override
    public int getItemCount() {
        return bookingModelsArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView room_name, price, mTitle;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            room_name = itemView.findViewById(R.id.room_name);
            price = itemView.findViewById(R.id.price);
            mTitle = itemView.findViewById(R.id.mTitle);


        }
    }


    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}