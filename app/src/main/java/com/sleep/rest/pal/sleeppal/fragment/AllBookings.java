package com.sleep.rest.pal.sleeppal.fragment;

import static com.sleep.rest.pal.sleeppal.MainScreen.mBack;
import static com.sleep.rest.pal.sleeppal.MainScreen.mPerson;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.MainScreen;
import com.sleep.rest.pal.sleeppal.adapter.AdapterCheckIN;
import com.sleep.rest.pal.sleeppal.adapter.SelectCitySpinner;
import com.sleep.rest.pal.sleeppal.model.CheckINModels;
import com.sleep.rest.pal.sleeppal.model.CityModel;
import com.sleep.rest.pal.sleeppal.utils.EndlessRecyclerViewScrollListener;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.Utils;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AllBookings extends Fragment {

    private AdapterCheckIN mAdapterUnits;
    private RecyclerView mRecyclerView;
    private ArrayList<CheckINModels> mUnitListModels = new ArrayList<>();
    private RequestQueue requestQueue;
    private UserSession mUserSession;
    private TextView mTotalCount, checkInText, checkOutText, statusText;
    private EditText searchET;
    private LinearLayout filterBtn;

    // pagination
    private int mPage = 1;
    private int last_size;
    private LinearLayoutManager mLinearLayoutManager;

    private BottomSheetDialog dialogFilters;
    private String startInStr = "Start Date", endInStr = "End Date", startOutStr = "Start Date", endOutTVStr = "End Date", statusStr = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_bookings, container, false);

        requestQueue = Volley.newRequestQueue(requireActivity());
        mUserSession = new UserSession(requireActivity());

        mTotalCount = view.findViewById(R.id.mTotalCount);
        mRecyclerView = view.findViewById(R.id.mRecyclerView);
        checkInText = view.findViewById(R.id.checkInText);
        checkOutText = view.findViewById(R.id.checkOutText);
        statusText = view.findViewById(R.id.statusText);
        filterBtn = view.findViewById(R.id.filterBtn);
        searchET = view.findViewById(R.id.searchET);

        MainScreen.mTitle.setText("All Bookings");
        mPerson.setVisibility(View.VISIBLE);
        mBack.setVisibility(View.INVISIBLE);


        searchET.setFocusableInTouchMode(true);
        searchET.requestFocus();
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(searchET, InputMethodManager.SHOW_IMPLICIT);

        searchET.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mUnitListModels.clear();
                    mPage = 1;
                    getALlBookings(searchET.getText().toString().trim(), startInStr, endInStr, startOutStr, endOutTVStr, statusStr, mPage);
                    imm.hideSoftInputFromWindow(searchET.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        mLinearLayoutManager = new LinearLayoutManager(requireActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mAdapterUnits = new AdapterCheckIN(getActivity(), mUnitListModels, new AdapterCheckIN.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                MainScreen.mTitle.setText("All Bookings Details");
                Staying_details fragobj = new Staying_details();
                Bundle bundle = new Bundle();
                bundle.putString("OrderID", mUnitListModels.get(item).getOrderID());
                fragobj.setArguments(bundle);
                switchFragment(R.id.fragment_layout, fragobj, "StayingDetailsFragment");
            }
        });
        mRecyclerView.setAdapter(mAdapterUnits);
        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {

                if (mPage != last_size) {
                    mPage++;
                    getALlBookings(searchET.getText().toString().trim(), startInStr, endInStr, startOutStr, endOutTVStr, statusStr, mPage);
                }
            }
        });

        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFilters.show();
            }
        });


        dialogFilters(requireActivity());
        if (mUnitListModels.isEmpty()) {
            getALlBookings(searchET.getText().toString().trim(), "Start Date", "End Date", "Start Date", "End Date", "", mPage);
        }

        return view;
    }


    private void dialogFilters(Context context) {
        dialogFilters = new BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme);
        dialogFilters.setContentView(R.layout.bottom_filters);

        Spinner spinnerStatus = dialogFilters.findViewById(R.id.spinnerStatus);
        ImageView closeImg = dialogFilters.findViewById(R.id.closeImg);
        TextView saveBtn = dialogFilters.findViewById(R.id.saveBtn);
        TextView clearFilterTV = dialogFilters.findViewById(R.id.clearFilterTV);

        RelativeLayout startInLayout = dialogFilters.findViewById(R.id.startInLayout);
        RelativeLayout endInLayout = dialogFilters.findViewById(R.id.endInLayout);
        TextView startInTV = dialogFilters.findViewById(R.id.startInTV);
        TextView endInTV = dialogFilters.findViewById(R.id.endInTV);

        RelativeLayout startOutLayout = dialogFilters.findViewById(R.id.startOutLayout);
        RelativeLayout endOutLayout = dialogFilters.findViewById(R.id.endOutLayout);
        TextView startOutTV = dialogFilters.findViewById(R.id.startOutTV);
        TextView endOutTV = dialogFilters.findViewById(R.id.endOutTV);

        ArrayList<CityModel> mSelectRole = new ArrayList<>();
        mSelectRole.add(new CityModel("new", "New"));
        mSelectRole.add(new CityModel("cancelled", "Cancelled"));
        mSelectRole.add(new CityModel("modified", "Modified"));
        mSelectRole.add(new CityModel("", "Select Status"));

        SelectCitySpinner adapter = new SelectCitySpinner(requireActivity(), R.layout.simple_spinner_item, mSelectRole);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinnerStatus.setAdapter(adapter);
        spinnerStatus.setSelection(adapter.getCount());

        startInLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showDatePickerDialog(context, startInTV, "");
            }
        });
        endInLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!startInTV.getText().toString().equals("Start Date")) {
                    Utils.showDatePickerDialog(context, endInTV, startInTV.getText().toString());
                } else {
                    Toast.makeText(context, "Select Start Date", Toast.LENGTH_SHORT).show();
                }
            }
        });
        startOutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showDatePickerDialog(context, startOutTV, "");
            }
        });
        endOutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!startOutTV.getText().toString().equals("Start Date")) {
                    Utils.showDatePickerDialog(context, endOutTV, startOutTV.getText().toString());
                } else {
                    Toast.makeText(context, "Select Start Date", Toast.LENGTH_SHORT).show();
                }
            }
        });

        clearFilterTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startInTV.setText("Start Date");
                endInTV.setText("End Date");
                startOutTV.setText("Start Date");
                endOutTV.setText("End Date");
                spinnerStatus.setSelection(mSelectRole.size() - 1);

                startInStr = startInTV.getText().toString();
                endInStr = endInTV.getText().toString();
                startOutStr = startOutTV.getText().toString();
                endOutTVStr = endOutTV.getText().toString();
                statusStr = mSelectRole.get(spinnerStatus.getSelectedItemPosition()).getCityId();
            }
        });

        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFilters.dismiss();
            }
        });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!startInTV.getText().toString().equals("Start Date") && endInTV.getText().toString().equals("End Date")) {
                    Toast.makeText(context, "Select End Date in", Toast.LENGTH_SHORT).show();
                } else if (!startOutTV.getText().toString().equals("Start Date") && endOutTV.getText().toString().equals("End Date")) {
                    Toast.makeText(context, "Select End Date out", Toast.LENGTH_SHORT).show();
                } else {
                    startInStr = startInTV.getText().toString();
                    endInStr = endInTV.getText().toString();
                    startOutStr = startOutTV.getText().toString();
                    endOutTVStr = endOutTV.getText().toString();
                    statusStr = mSelectRole.get(spinnerStatus.getSelectedItemPosition()).getCityId();

//                    mUnitListModels.clear();
//                    getALlBookings(searchET.getText().toString().trim(), startInStr, endInStr, startOutStr, endOutTVStr, statusStr, mPage);
                    dialogFilters.dismiss();
                }
            }
        });

        dialogFilters.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                mPage = 1;
                mUnitListModels.clear();
                mAdapterUnits.notifyDataSetChanged();
                getALlBookings(searchET.getText().toString().trim(), startInStr, endInStr, startOutStr, endOutTVStr, statusStr, mPage);
            }
        });

    }

    private void getALlBookings(String search, String startIn, String endIn, String startOut, String endOut, String status, int mPage) {
        final KProgressHUD progressDialog = KProgressHUD.create(requireActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        StringBuilder url = new StringBuilder();
        if (!search.isEmpty()) {
            url.append("&referral_order_id=" + search);
        }
        if (!endIn.equals("End Date")) {
            url.append("&checkin_start_date=" + startIn + "&checkin_end_date=" + endIn);
            checkInText.setText("Check In : " + startIn + " to " + endIn);
            checkInText.setVisibility(View.VISIBLE);
        } else {
            checkInText.setVisibility(View.GONE);
        }
        if (!endOut.equals("End Date")) {
            url.append("&checkout_start_date=" + startOut + "&checkout_end_date=" + endOut);
            checkOutText.setText("Check Out : " + startOut + " to " + endOut);
            checkOutText.setVisibility(View.VISIBLE);
        } else {
            checkOutText.setVisibility(View.GONE);
        }
        if (!status.isEmpty()) {
            url.append("&status=" + status);
            statusText.setText("Status : " + status);
            statusText.setVisibility(View.VISIBLE);
        } else {
            statusText.setVisibility(View.GONE);
        }

        Log.e("getUrlFilter", url.toString());

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-all-booking?page=" + mPage + url,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("getAllBookings", jsonObject.toString());

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject paginationObj = jsonObject.getJSONObject("pagination");
                                last_size = paginationObj.getInt("last_page");

                                JSONArray jsonArray1 = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject object = jsonArray1.getJSONObject(i);

                                    CheckINModels listModels = new CheckINModels();
                                    listModels.setRoomName(object.getString("room_name"));
                                    listModels.setGuestName(object.getString("guests"));
                                    listModels.setOrderID(object.getString("order_id"));
                                    listModels.setNights_No(object.getString("nights"));
                                    listModels.setPrice("IDR " + object.getString("price"));
                                    listModels.setTotalPrice("IDR " + object.getString("total"));
                                    listModels.setCheckIN("Check In " + getDate(object.getString("checkin")));
                                    listModels.setCheckOUT("Check Out " + getDate(object.getString("checkout")));
                                    listModels.setReferral("Referral " + object.getString("referral"));
                                    listModels.setLogo(object.getString("ota_logo_url"));

                                    mUnitListModels.add(listModels);
                                }
                                mAdapterUnits.notifyDataSetChanged();

                                mTotalCount.setText("" + mUnitListModels.size());

                            } else {
                                Toast.makeText(requireActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Log.e("Exception", e.getMessage());
                            Toast.makeText(requireActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
//                        if (error.networkResponse.statusCode == 401) {
//                            mUserSession.logout();
//                            startActivity(new Intent(getActivity(), LoginScreen.class));
//                            getActivity().finishAffinity();
//                        }
                        if (error instanceof ServerError)
                            Toast.makeText(requireActivity(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(requireActivity(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(requireActivity(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);
    }

    private String getDate(String mDate) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
            LocalDate date = LocalDate.parse(mDate, inputFormatter);
            return outputFormatter.format(date);
        } else {
            return mDate;
        }
    }

    protected void switchFragment(@IdRes int containerViewId,
                                  @NonNull Fragment newFragment,
                                  @NonNull String fragmentTag) {
        FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        // Get currently visible fragment
        Fragment currentFragment = fragmentManager.findFragmentById(containerViewId);
        if (currentFragment != null) {
            transaction.hide(currentFragment); // Hide the current fragment
        }

        // Check if the new fragment already exists
        Fragment existingFragment = fragmentManager.findFragmentByTag(fragmentTag);
        if (existingFragment != null) {
            transaction.show(existingFragment); // Show existing fragment
        } else {
            transaction.add(containerViewId, newFragment, fragmentTag); // Add new fragment
            transaction.addToBackStack(fragmentTag); // Add to back stack
        }
        transaction.commit();
    }


    public void onFragmentResume() {
        MainScreen.mTitle.setText("All Bookings");
        mPerson.setVisibility(View.VISIBLE);
        mBack.setVisibility(View.INVISIBLE);
    }

}