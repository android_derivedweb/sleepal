package com.sleep.rest.pal.sleeppal.model;

public class CheckINModels {
    private String roomName;
    private String nights_No;
    private String price;
    private String totalPrice;
    private String checkIN;
    private String checkOUT;
    private String referral;
    private String guestName;
    private String orderID;
    private String logo;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setNights_No(String nights_no) {
        this.nights_No = nights_no;
    }

    public String getNights_No() {
        return nights_No;
    }

    public void setPrice(String price) {

        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setCheckIN(String checkIN) {
        this.checkIN = checkIN;
    }

    public String getCheckIN() {
        return checkIN;
    }

    public void setCheckOUT(String checkOUT) {
        this.checkOUT = checkOUT;
    }

    public String getCheckOUT() {
        return checkOUT;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getReferral() {
        return referral;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getOrderID() {
        return orderID;
    }
}
