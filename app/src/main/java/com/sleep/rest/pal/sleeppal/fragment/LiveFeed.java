package com.sleep.rest.pal.sleeppal.fragment;

import static com.sleep.rest.pal.sleeppal.MainScreen.mBack;
import static com.sleep.rest.pal.sleeppal.MainScreen.mPerson;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.MainScreen;
import com.sleep.rest.pal.sleeppal.adapter.AdapterLifeFeed;
import com.sleep.rest.pal.sleeppal.model.LiveFeedModel;
import com.sleep.rest.pal.sleeppal.utils.EndlessRecyclerViewScrollListener;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LiveFeed extends Fragment {

    private AdapterLifeFeed adapterLifeFeed;
    private RecyclerView mRecyclerView;
    private ArrayList<LiveFeedModel> liveFeedArrayList = new ArrayList<>();
    private RequestQueue requestQueue;
    private UserSession mUserSession;
    private TextView mTotalCount;

    // pagination
    private int mPage = 1;
    private int last_size;
    private LinearLayoutManager mLinearLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live_feed, container, false);

        requestQueue = Volley.newRequestQueue(requireActivity());
        mUserSession = new UserSession(requireActivity());

        mTotalCount = view.findViewById(R.id.mTotalCount);
        mRecyclerView = view.findViewById(R.id.mRecyclerView);

        MainScreen.mTitle.setText("Live Feed");
        mPerson.setVisibility(View.VISIBLE);
        mBack.setVisibility(View.INVISIBLE);

        mLinearLayoutManager = new LinearLayoutManager(requireActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        adapterLifeFeed = new AdapterLifeFeed(getActivity(), liveFeedArrayList, new AdapterLifeFeed.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

//                MainScreen.mTitle.setText("Live Feed Details");
//                Staying_details fragobj = new Staying_details();
//                Bundle bundle = new Bundle();
//                bundle.putString("OrderID", liveFeedArrayList.get(item).getOrderID());
//                fragobj.setArguments(bundle);
//                switchFragment(R.id.fragment_layout, fragobj, "StayingDetailsFragment");
            }
        });
        mRecyclerView.setAdapter(adapterLifeFeed);
        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {

                if (mPage != last_size) {
                    mPage++;
                    getLiveFeed(mPage);
                }
            }
        });

        if (liveFeedArrayList.isEmpty()) {
            getLiveFeed(mPage);
        }

        return view;
    }

    private void getLiveFeed(int mPage) {
        final KProgressHUD progressDialog = KProgressHUD.create(requireActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "owner/get-live-feed-data?page=" + mPage,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("getLiveFeed", jsonObject.toString());

//                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject paginationObj = jsonObject.getJSONObject("pagination");
                                last_size = paginationObj.getInt("total_pages");

                                JSONArray jsonArray1 = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject object = jsonArray1.getJSONObject(i);

                                    LiveFeedModel model = new LiveFeedModel();
                                    model.setLive_booking_feed_id(object.getString("live_booking_feed_id"));
                                    model.setBooking_id(object.getString("booking_id"));
                                    model.setRoom_id(object.getString("room_id"));
                                    model.setGuests(object.getString("guests"));
                                    model.setNights(object.getString("nights"));
                                    model.setName(object.getString("name"));
                                    model.setPrice(object.getString("price"));
                                    model.setReferral(object.getString("referral"));
                                    model.setStatus(object.getString("status"));
                                    model.setCheckin(object.getString("checkin"));
                                    model.setCreated_at(object.getString("created_at"));
                                    model.setTotal(object.getString("total"));
                                    liveFeedArrayList.add(model);
                                }
                                adapterLifeFeed.notifyDataSetChanged();

                                mTotalCount.setText("" + liveFeedArrayList.size());

//                            } else {
//                                Toast.makeText(requireActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
//                            }

                        } catch (Exception e) {
                            Log.e("Exception", e.getMessage());
                            Toast.makeText(requireActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
//                        if (error.networkResponse.statusCode == 401) {
//                            mUserSession.logout();
//                            startActivity(new Intent(getActivity(), LoginScreen.class));
//                            getActivity().finishAffinity();
//                        }
                        if (error instanceof ServerError)
                            Toast.makeText(requireActivity(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(requireActivity(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(requireActivity(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);
    }


    protected void switchFragment(@IdRes int containerViewId,
                                  @NonNull Fragment newFragment,
                                  @NonNull String fragmentTag) {
        FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        // Get currently visible fragment
        Fragment currentFragment = fragmentManager.findFragmentById(containerViewId);
        if (currentFragment != null) {
            transaction.hide(currentFragment); // Hide the current fragment
        }

        // Check if the new fragment already exists
        Fragment existingFragment = fragmentManager.findFragmentByTag(fragmentTag);
        if (existingFragment != null) {
            transaction.show(existingFragment); // Show existing fragment
        } else {
            transaction.add(containerViewId, newFragment, fragmentTag); // Add new fragment
            transaction.addToBackStack(fragmentTag); // Add to back stack
        }
        transaction.commit();
    }


    public void onFragmentResume() {
        MainScreen.mTitle.setText("Live Feed");
        mPerson.setVisibility(View.VISIBLE);
        mBack.setVisibility(View.INVISIBLE);
    }

}