package com.sleep.rest.pal.sleeppal.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.model.AddonServiceModel;

import java.util.ArrayList;

public class AdapterAddOnService extends RecyclerView.Adapter<AdapterAddOnService.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;

    private ArrayList<AddonServiceModel> serviceModelArrayList;


    public AdapterAddOnService(Context mContext, ArrayList<AddonServiceModel> serviceModelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.serviceModelArrayList = serviceModelArrayList;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_addon_service, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
        holder.serviceName.setText(serviceModelArrayList.get(position).getAddon_service_name());
        holder.amount.setText(serviceModelArrayList.get(position).getAmount());
        holder.transactionType.setText(serviceModelArrayList.get(position).getTransaction_type());

    }

    @Override
    public int getItemCount() {
        return serviceModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView serviceName, amount, transactionType;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            serviceName = itemView.findViewById(R.id.serviceName);
            amount = itemView.findViewById(R.id.amount);
            transactionType = itemView.findViewById(R.id.transactionType);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}