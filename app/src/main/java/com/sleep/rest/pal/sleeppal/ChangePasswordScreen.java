package com.sleep.rest.pal.sleeppal;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.sleep.pal.sleeppal.R;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordScreen extends AppCompatActivity {

    private TextView mDone;
    private EditText mPassword1,mPassword2;

    private RequestQueue requestQueue;
    private UserSession mUserSession;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newpassword);
        requestQueue = Volley.newRequestQueue(ChangePasswordScreen.this);//Creating the RequestQueue
        mUserSession = new UserSession(ChangePasswordScreen.this);


        mPassword1 = findViewById(R.id.new_pass);
        mPassword2 = findViewById(R.id.new_pass2);
        mDone = findViewById(R.id.done);

        Log.e("sadsad",mUserSession.getUserID());
        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mPassword1.getText().toString().trim().isEmpty()){
                    Toast.makeText(ChangePasswordScreen.this, "Please Enter Your Password!!!", Toast.LENGTH_SHORT).show();
                }else if(mPassword2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(ChangePasswordScreen.this, "Please Enter Your Confrim Password!!!", Toast.LENGTH_SHORT).show();
                }else if(!mPassword1.getText().toString().equals(mPassword2.getText().toString())){
                    Toast.makeText(ChangePasswordScreen.this, "Passwords do NOT match", Toast.LENGTH_SHORT).show();
                }else {
                    ResetCodePasswordRequest(mPassword1.getText().toString());
                }
            }
        });
    }

    private void ResetCodePasswordRequest(String Password) {

        final KProgressHUD progressDialog = KProgressHUD.create(ChangePasswordScreen.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL+"change-password",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if(jsonObject.getString("ResponseCode").equals("200")){
                                Toast.makeText(ChangePasswordScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                finish();
                            }else {
                                Toast.makeText(ChangePasswordScreen.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(ChangePasswordScreen.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if(error.networkResponse.statusCode == 401){
                            mUserSession.logout();
                            startActivity(new Intent(ChangePasswordScreen.this, LoginScreen.class));
                            finishAffinity();

                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(ChangePasswordScreen.this, R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(ChangePasswordScreen.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(ChangePasswordScreen.this, R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("owner_id", mUserSession.getUserID());
                params.put("new_password", Password);
                params.put("confirm_password", Password);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
               // params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



}