package com.sleep.rest.pal.sleeppal.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.model.CityModel;

import java.util.ArrayList;

public class SelectCitySpinner extends ArrayAdapter<CityModel> {

    private Context context;
    private ArrayList<CityModel> values;

    public SelectCitySpinner(Context context, int textViewResourceId, ArrayList<CityModel> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        int count = super.getCount();
        return count > 0 ? count - 1 : count;
        //return values.size();
    }

    @Override
    public CityModel getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView label = (TextView) super.getView(position, convertView, parent);
//        label.setTextSize(context.getResources().getDimension(com.intuit.sdp.R.dimen._5sdp));
//        label.setTextColor(context.getResources().getColor(R.color.sky_blue));
        label.setText(values.get(position).getCityname());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
//        label.setTextSize(context.getResources().getDimension(com.intuit.sdp.R.dimen._5sdp));
//        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getCityname());
        return label;
    }
}