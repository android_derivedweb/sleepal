package com.sleep.rest.pal.sleeppal.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.sleep.pal.sleeppal.R;

import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.model.AccountModels;

import java.util.ArrayList;

public class AdapterAccount extends RecyclerView.Adapter<AdapterAccount.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;

    private ArrayList<AccountModels> bookingModelsArrayList;



    public AdapterAccount(Context mContext, ArrayList<AccountModels> bookingModelsArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.bookingModelsArrayList = bookingModelsArrayList;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_account, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
        holder.mRoomName.setText(bookingModelsArrayList.get(position).getRoom_name());
        holder.mCheckin_cnt.setText(bookingModelsArrayList.get(position).getChecked_cnt());
        holder.mNight_cnt.setText(bookingModelsArrayList.get(position).getNights_cnt());
        holder.mEarning.setText(bookingModelsArrayList.get(position).getEarning_cnt());
        holder.mExpense.setText(bookingModelsArrayList.get(position).getExpense_cnt());
        holder.mTotal.setText(bookingModelsArrayList.get(position).getTotal_cnt());








    }

    @Override
    public int getItemCount() {
        return bookingModelsArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView mRoomName, mCheckin_cnt,mNight_cnt,mEarning,mExpense,mTotal;



        public Viewholder(@NonNull View itemView) {
            super(itemView);

            mRoomName = itemView.findViewById(R.id.mRoomName);
            mCheckin_cnt = itemView.findViewById(R.id.mCheckin_cnt);
            mNight_cnt = itemView.findViewById(R.id.mNight_cnt);
            mEarning = itemView.findViewById(R.id.mEarning);
            mExpense = itemView.findViewById(R.id.mExpense);
            mTotal = itemView.findViewById(R.id.mTotal);


        }
    }


    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}