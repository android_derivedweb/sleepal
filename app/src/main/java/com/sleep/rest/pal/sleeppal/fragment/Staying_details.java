package com.sleep.rest.pal.sleeppal.fragment;

import static com.sleep.rest.pal.sleeppal.MainScreen.mBack;
import static com.sleep.rest.pal.sleeppal.MainScreen.mPerson;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.rest.pal.sleeppal.LoginScreen;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.adapter.AdapterAddOnService;
import com.sleep.rest.pal.sleeppal.model.AddonServiceModel;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Staying_details extends Fragment {

    private RequestQueue requestQueue;
    private UserSession mUserSession;

    private TextView mDate, room_name, guests, nights, price, total, checkin, checkout,
            referral, name, address, country, email, phone, comment, price2, total2, invoice, referralOrderId, status;

    private ImageView bookingIcon;

    private String OrderID;

    private RecyclerView recAddOnService;
    private AdapterAddOnService adapterAddOnService;
    private ArrayList<AddonServiceModel> serviceModelArrayList = new ArrayList<>();
    private LinearLayout addOnServiceLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_staying_details, container, false);

        requestQueue = Volley.newRequestQueue(requireActivity());//Creating the RequestQueue
        mUserSession = new UserSession(requireActivity());

        try {
            OrderID = getArguments().getString("OrderID");
        } catch (Exception e) {

        }

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        mDate = view.findViewById(R.id.mDate);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        mDate.setText("Date : " + formattedDate);

        mPerson.setVisibility(View.INVISIBLE);
        mBack.setVisibility(View.VISIBLE);

        room_name = view.findViewById(R.id.room_name);
        guests = view.findViewById(R.id.guests);
        nights = view.findViewById(R.id.nights);
        price = view.findViewById(R.id.price);
        total = view.findViewById(R.id.total);
        checkin = view.findViewById(R.id.checkin);
        checkout = view.findViewById(R.id.checkout);
        referral = view.findViewById(R.id.referral);
        name = view.findViewById(R.id.name);
        address = view.findViewById(R.id.address);
        country = view.findViewById(R.id.country);
        email = view.findViewById(R.id.email);
        phone = view.findViewById(R.id.phone);
        comment = view.findViewById(R.id.comment);
        price2 = view.findViewById(R.id.price2);
        total2 = view.findViewById(R.id.total2);
        invoice = view.findViewById(R.id.invoice);
        recAddOnService = view.findViewById(R.id.recAddOnService);
        addOnServiceLayout = view.findViewById(R.id.addOnServiceLayout);
        bookingIcon = view.findViewById(R.id.bookingIcon);
        referralOrderId = view.findViewById(R.id.referralOrderId);
        status = view.findViewById(R.id.status);

        recAddOnService.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterAddOnService = new AdapterAddOnService(getActivity(), serviceModelArrayList, new AdapterAddOnService.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


            }
        });
        recAddOnService.setAdapter(adapterAddOnService);


        DetailsRequest(OrderID);

        return view;
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();

    }


    private String getDate(String mDate) {

        DateTimeFormatter inputFormatter = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
            LocalDate date = LocalDate.parse(mDate, inputFormatter);
            String appDate = outputFormatter.format(date);

            return appDate;
        } else {
            return mDate;
        }
    }

    private void DetailsRequest(String mID) {
        final KProgressHUD progressDialog = KProgressHUD.create(requireActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL
                + "get-checkin-details?order_id=" + mID,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("getDetailInfo", jsonObject.toString());
                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                JSONArray jsonArray = jsonObject1.getJSONArray("details");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    Glide.with(getActivity()).load(object.getString("ota_logo_url")).into(bookingIcon);

                                    room_name.setText(object.getString("room_name"));
                                    guests.setText(object.getString("guests"));
                                    nights.setText(object.getString("nights"));
                                    price.setText("" + object.getString("price"));
                                    total.setText("" + object.getString("total"));
                                    checkin.setText("Check In " + getDate(object.getString("checkin")));
                                    checkout.setText("Check Out " + getDate(object.getString("checkout")));
                                    referral.setText("Referral " + object.getString("referral"));
                                    referralOrderId.setText(object.getString("referral_order_id"));
                                    status.setText(object.getString("status"));
                                    status.setTextColor(Color.parseColor(object.getString("status_color")));

                                    name.setText(object.getString("name"));
                                    address.setText(object.getString("address"));
                                    country.setText(object.getString("country"));
                                    email.setText(object.getString("email"));
                                    phone.setText(object.getString("phone"));
                                    comment.setText(object.getString("comment"));
                                    price2.setText(object.getString("total"));
                                    total2.setText(object.getString("balance"));
                                    invoice.setText(object.getString("invoice"));

                                    JSONArray addOnJArray = jsonArray.getJSONObject(0).getJSONArray("addon_services");

                                    for (int j = 0; j < addOnJArray.length(); j++) {
                                        JSONObject object1 = addOnJArray.getJSONObject(j);

                                        AddonServiceModel model = new AddonServiceModel();
                                        model.setAddon_service_id(object1.getString("addon_service_id"));
                                        model.setAddon_service_name(object1.getJSONObject("addon_service_type").getString("service_name"));
                                        model.setAmount(object1.getString("addon_amount"));
                                        model.setTransaction_type(object1.getString("transaction_type"));
                                        serviceModelArrayList.add(model);
                                    }
                                    adapterAddOnService.notifyDataSetChanged();

                                    if (!serviceModelArrayList.isEmpty()) {
                                        addOnServiceLayout.setVisibility(View.VISIBLE);
                                    }

                                }
                            } else {
                                Toast.makeText(requireActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(requireActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(getActivity(), LoginScreen.class));
                            getActivity().finishAffinity();

                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(requireActivity(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(requireActivity(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(requireActivity(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);
    }


}