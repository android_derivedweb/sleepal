package com.sleep.rest.pal.sleeppal.model;

public class UnitListModels {
    private String roomName;
    private String checked_CNT;
    private String nights_CNT;
    private String earning_CNT;
    private String expense_CNT;
    private String total_CNT;
    private String room_ID;
    private String orderID;
    private String price;
    private String total;
    private String checkIn;
    private String checkOut;
    private String night;
    private String guest;
    private String referral;
    private String status;
    private String name;
    private String description;
    private String logo;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setChecked_CNT(String checked_cnt) {
        this.checked_CNT = checked_cnt;
    }

    public String getChecked_CNT() {
        return checked_CNT;
    }

    public void setNights_CNT(String nights_cnt) {
        this.nights_CNT = nights_cnt;
    }

    public String getNights_CNT() {
        return nights_CNT;
    }

    public void setEarning_CNT(String earning_cnt) {
        this.earning_CNT = earning_cnt;
    }

    public String getEarning_CNT() {
        return earning_CNT;
    }

    public void setExpense_CNT(String expense_cnt) {
        this.expense_CNT = expense_cnt;
    }

    public String getExpense_CNT() {
        return expense_CNT;
    }

    public void setTotal_CNT(String total_cnt) {
        this.total_CNT = total_cnt;
    }

    public String getTotal_CNT() {
        return total_CNT;
    }

    public void setRoom_ID(String room_id) {
        this.room_ID = room_id;
    }

    public String getRoom_ID() {
        return room_ID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setNight(String night) {
        this.night = night;
    }

    public String getNight() {
        return night;
    }

    public void setGuest(String guest) {
        this.guest = guest;
    }

    public String getGuest() {
        return guest;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getReferral() {
        return referral;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
