package com.sleep.rest.pal.sleeppal.model;

public class AccountListModel {

    private String title;
    private String checkin;
    private String value;
    private String addon_amount;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAddon_amount() {
        return addon_amount;
    }

    public void setAddon_amount(String addon_amount) {
        this.addon_amount = addon_amount;
    }

}
