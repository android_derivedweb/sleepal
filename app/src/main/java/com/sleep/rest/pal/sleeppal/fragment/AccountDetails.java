package com.sleep.rest.pal.sleeppal.fragment;

import static com.sleep.rest.pal.sleeppal.MainScreen.mBack;
import static com.sleep.rest.pal.sleeppal.MainScreen.mPerson;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.rest.pal.sleeppal.LoginScreen;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.adapter.AdapterAccountCheckin;
import com.sleep.rest.pal.sleeppal.adapter.AdapterAccountExpense;
import com.sleep.rest.pal.sleeppal.model.AccountListModel;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AccountDetails extends Fragment {

    private static TextView fromDate, toDate;
    private static String fromDateStr, toDateStr;
    private String OrderID;
    private String fromDate1;
    private String toDate1;
    private RequestQueue requestQueue;
    private UserSession mUserSession;
    private TextView mRoomName;
    private TextView mTotalEarning;
    private TextView mTotalExpense;
    private TextView mGrandTotal;
    private ArrayList<AccountListModel> mListEarning = new ArrayList<>();
    private ArrayList<AccountListModel> mListExpense = new ArrayList<>();
    private RecyclerView mRecyclerView_1;
    private AdapterAccountCheckin mRecyclerView_1_Adapter;
    private RecyclerView mRecyclerView_2;
    private AdapterAccountExpense mRecyclerView_2_Adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_details, container, false);
        requestQueue = Volley.newRequestQueue(requireActivity());//Creating the RequestQueue
        mUserSession = new UserSession(requireActivity());

        try {
            OrderID = getArguments().getString("OrderID");
            fromDate1 = getArguments().getString("fromDate");
            toDate1 = getArguments().getString("toDate");

        } catch (Exception e) {

        }

        if (fromDate1.equals("Select date")) {
            AccountRequestDetails(OrderID);
        } else {
            AccountRequestDetails(OrderID, fromDate1, toDate1);
        }

        mPerson.setVisibility(View.INVISIBLE);
        mBack.setVisibility(View.VISIBLE);

        mRoomName = view.findViewById(R.id.mRoomName);
        mTotalEarning = view.findViewById(R.id.mTotalEarning);
        mTotalExpense = view.findViewById(R.id.mTotalExpense);
        mGrandTotal = view.findViewById(R.id.mGrandTotal);

        mRecyclerView_1 = view.findViewById(R.id.mRecyclerView_1);
        mRecyclerView_1.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView_1_Adapter = new AdapterAccountCheckin(getActivity(), mListEarning, new AdapterAccountCheckin.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


            }
        });
        mRecyclerView_1.setAdapter(mRecyclerView_1_Adapter);

        mRecyclerView_2 = view.findViewById(R.id.mRecyclerView_2);
        mRecyclerView_2.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView_2_Adapter = new AdapterAccountExpense(getActivity(), mListExpense, new AdapterAccountExpense.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


            }
        });
        mRecyclerView_2.setAdapter(mRecyclerView_2_Adapter);

        return view;
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();

    }

    private String getDate(String mDate) {

        DateTimeFormatter inputFormatter = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
            LocalDate date = LocalDate.parse(mDate, inputFormatter);
            String appDate = outputFormatter.format(date);
            return appDate;
        } else {
            return mDate;
        }
    }

    private void AccountRequestDetails(String OrderID, String start_date, String to_date) {
        final KProgressHUD progressDialog = KProgressHUD.create(requireActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-accounting-details?room_id=" + OrderID + "&from_date=" + start_date + "&to_date=" + to_date,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {
                                Log.e("eweqwe", jsonObject.toString());
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                mRoomName.setText(jsonArray.getJSONObject(0).getString("nobeds_room_name"));
                                mTotalEarning.setText(jsonArray.getJSONObject(3).getJSONArray("total").getJSONObject(2).getString("total_earning"));
                                mTotalExpense.setText(jsonArray.getJSONObject(3).getJSONArray("total").getJSONObject(1).getString("total_expense"));
                                mGrandTotal.setText(jsonArray.getJSONObject(3).getJSONArray("total").getJSONObject(0).getString("grand_total"));
                                JSONArray jsonArray1 = jsonArray.getJSONObject(1).getJSONArray("Expenses");
                                JSONArray jsonArray2 = jsonArray.getJSONObject(2).getJSONArray("Earning");


                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject object = jsonArray1.getJSONObject(i);
                                    AccountListModel listModels = new AccountListModel();
                                    listModels.setCheckin("Check in " + getDate(object.getString("checkin")));
                                    listModels.setTitle(object.getString("title"));
                                    listModels.setValue("IDR " + object.getString("value"));
                                    mListExpense.add(listModels);
                                }
                                mRecyclerView_2_Adapter.notifyDataSetChanged();
                                Log.e("qwqwqwqw", jsonArray2.toString());

                                for (int i = 0; i < jsonArray2.length(); i++) {
                                    JSONObject object = jsonArray2.getJSONObject(i);
                                    AccountListModel listModels = new AccountListModel();
                                    try {
                                        listModels.setCheckin("Check in " + getDate(object.getString("checkin")));
                                    } catch (Exception e) {
                                        listModels.setCheckin("Check in " + object.getString("checkin"));
                                    }
                                    // listModels.setCheckin("Check in "+ object.getString("checkin"));

                                    listModels.setTitle(object.getString("title"));
                                    listModels.setValue("IDR " + object.getString("value"));
                                    mListEarning.add(listModels);
                                }


                                mRecyclerView_1_Adapter.notifyDataSetChanged();
                                //    mAdapterUnits.notifyDataSetChanged();
                            } else {

                                Toast.makeText(requireActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            Log.e("Exception", e.getMessage());
                            Toast.makeText(requireActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(getActivity(), LoginScreen.class));
                            getActivity().finishAffinity();

                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(requireActivity(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(requireActivity(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(requireActivity(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                //  params.put("email", mEmail);
                // params.put("first_name", mName);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }

    private void AccountRequestDetails(String OrderID) {
        final KProgressHUD progressDialog = KProgressHUD.create(requireActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-accounting-details?room_id=" + OrderID,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {
                                Log.e("eweqwe", jsonObject.toString());
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                mRoomName.setText(jsonArray.getJSONObject(0).getString("nobeds_room_name"));
                                mTotalEarning.setText(jsonArray.getJSONObject(3).getJSONArray("total").getJSONObject(2).getString("total_earning"));
                                mTotalExpense.setText(jsonArray.getJSONObject(3).getJSONArray("total").getJSONObject(1).getString("total_expense"));
                                mGrandTotal.setText(jsonArray.getJSONObject(3).getJSONArray("total").getJSONObject(0).getString("grand_total"));
                                JSONArray jsonArray1 = jsonArray.getJSONObject(1).getJSONArray("Expenses");
                                JSONArray jsonArray2 = jsonArray.getJSONObject(2).getJSONArray("Earning");


                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject object = jsonArray1.getJSONObject(i);
                                    AccountListModel listModels = new AccountListModel();
                                    listModels.setCheckin("Check in " + getDate(object.getString("checkin")));
                                    listModels.setTitle(object.getString("title"));
                                    listModels.setValue("IDR " + object.getString("value"));
                                    listModels.setAddon_amount("IDR " + object.getString("addon_amount"));
                                    mListExpense.add(listModels);
                                }
                                mRecyclerView_2_Adapter.notifyDataSetChanged();
                                Log.e("qwqwqwqw", jsonArray2.toString());

                                for (int i = 0; i < jsonArray2.length(); i++) {
                                    JSONObject object = jsonArray2.getJSONObject(i);
                                    AccountListModel listModels = new AccountListModel();
                                    try {
                                        listModels.setCheckin("Check in " + getDate(object.getString("checkin")));
                                    } catch (Exception e) {
                                        listModels.setCheckin("Check in " + object.getString("checkin"));
                                    }
                                    listModels.setAddon_amount("IDR " + object.getString("addon_amount"));

                                    // listModels.setCheckin("Check in "+ object.getString("checkin"));

                                    listModels.setTitle(object.getString("title"));
                                    listModels.setValue("IDR " + object.getString("value"));
                                    mListEarning.add(listModels);
                                }


                                mRecyclerView_1_Adapter.notifyDataSetChanged();
                                //    mAdapterUnits.notifyDataSetChanged();
                            } else {

                                Toast.makeText(requireActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            Log.e("Exception", e.getMessage());
                            Toast.makeText(requireActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(getActivity(), LoginScreen.class));
                            getActivity().finishAffinity();

                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(requireActivity(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(requireActivity(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(requireActivity(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                //  params.put("email", mEmail);
                // params.put("first_name", mName);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


}