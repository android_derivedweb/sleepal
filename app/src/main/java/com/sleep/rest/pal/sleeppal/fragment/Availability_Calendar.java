package com.sleep.rest.pal.sleeppal.fragment;

import static com.sleep.rest.pal.sleeppal.MainScreen.mBack;
import static com.sleep.rest.pal.sleeppal.MainScreen.mPerson;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sleep.rest.pal.sleeppal.LoginScreen;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.adapter.AdapterUnits;
import com.sleep.rest.pal.sleeppal.adapter.AvailablityCalViewPager;
import com.sleep.rest.pal.sleeppal.model.UnitListModels;
import com.sleep.rest.pal.sleeppal.utils.UserSession;
import com.sleep.rest.pal.sleeppal.utils.VolleyMultipartRequest;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Availability_Calendar extends Fragment {
    // Store instance variablesversionDetails
    private CompactCalendarView compactCalendarView;
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yyyy", Locale.getDefault());
    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    private RequestQueue requestQueue;
    private UserSession mUserSession;
    private ArrayList<UnitListModels> mUnitListModels = new ArrayList<>();
    private JSONArray jsonArrayJSONArray;
    private RecyclerView mRecyclerView;
    private AdapterUnits mAdapterUnits;

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    // Inflate the view for the fragment based on layout XML
    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_availability_calendar, container, false);
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue
        mUserSession = new UserSession(getContext());


        TextView DateText = view.findViewById(R.id.month);
        TextView mCDate = view.findViewById(R.id.mCDate);
        compactCalendarView = (CompactCalendarView) view.findViewById(R.id.compactcalendar_view);
        compactCalendarView.setUseThreeLetterAbbreviation(true);

        Date c = Calendar.getInstance().getTime();
        String formattedDate = dateFormat.format(c);
        DateText.setText(formattedDate);

        mPerson.setVisibility(View.VISIBLE);
        mBack.setVisibility(View.INVISIBLE);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate1 = df.format(c);
        mCDate.setText("Date : " + formattedDate1);


      /*  mRecyclerView = view.findViewById(R.id.mRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapterUnits = new AdapterUnits(getActivity(), mUnitListModels, new AdapterUnits.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {



            }
        });
        mRecyclerView.setAdapter(mAdapterUnits);*/
        view.findViewById(R.id.right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.scrollRight();
            }
        });


        view.findViewById(R.id.left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.scrollLeft();
            }
        });
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                String selectedDateStr = DateFormat.format("yyyy-MM-dd", dateClicked).toString();
                // Toast.makeText(getContext(), selectedDateStr + "", Toast.LENGTH_SHORT).show();
                int month = dateClicked.getMonth() + 1;
                Calendar cal = Calendar.getInstance();
                cal.setTime(dateClicked);
                int yer = cal.get(Calendar.YEAR);
                mUnitListModels.clear();

                for (int i = 0; i < jsonArrayJSONArray.length(); i++) {
                    JSONObject object = null;
                    try {
                        object = jsonArrayJSONArray.getJSONObject(i);


                        if (getDate(object.getString("checkin_date")).equals(selectedDateStr)) {
                            UnitListModels listModels = new UnitListModels();
                            listModels.setRoomName(object.getString("room_name"));
                            listModels.setPrice(object.getString("price"));
                            listModels.setName(object.getString("name"));
                            listModels.setTotal(object.getString("total"));
                            listModels.setCheckIn(getDate(object.getString("checkin_date")));
                            listModels.setCheckOut(getDate(object.getString("checkout_date")));
                            listModels.setNight(object.getString("nights"));
                            listModels.setGuest(object.getString("guests"));
                            listModels.setReferral(object.getString("referral"));
                            listModels.setOrderID(object.getString("referral_order_id"));
                            listModels.setStatus(object.getString("status"));

                            mUnitListModels.add(listModels);
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

                if (mUnitListModels.size() > 0) {

                    BottomSheetDialog dialog = new BottomSheetDialog(getActivity(), R.style.BottomSheetDialog);
                    dialog.setContentView(R.layout.bottom_dialog_calender);
                    ViewPager mViewPager = (ViewPager) dialog.findViewById(R.id.viewPagerSeries);
                    DotsIndicator dotsIndicator = (DotsIndicator) dialog.findViewById(R.id.worm_dots_indicator);
                    AvailablityCalViewPager availablityCalViewPager = new AvailablityCalViewPager(getContext(), mUnitListModels, new AvailablityCalViewPager.OnItemClickListener() {
                        @Override
                        public void onItemClick(int item) {
                            dialog.dismiss();
                        }
                    });
                    mViewPager.setAdapter(availablityCalViewPager);
                    dotsIndicator.setViewPager(mViewPager);
                    dialog.show();
                    if (mUnitListModels.size() > 1) {
                        Toast.makeText(getActivity(), "Swap right or left to see other booking.", Toast.LENGTH_SHORT).show();
                    }

                    if (mUnitListModels.isEmpty()) {

                        Toast.makeText(getActivity(), "There is no any booing on this date.", Toast.LENGTH_SHORT).show();

                    }

                }
                //  mAdapterUnits.notifyDataSetChanged();
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                String formattedDate = dateFormat.format(firstDayOfNewMonth);
                DateText.setText(formattedDate);
                String formattedDate1 = dateFormat1.format(firstDayOfNewMonth);
                String formattedDate2 = dateFormat1.format(firstDayOfNewMonth);


                Date date = null;
                try {
                    date = dateFormat1.parse(formattedDate1);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    Log.e("1212", "" + calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    formattedDate2 = formattedDate2.substring(0, formattedDate2.length() - 2) + calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                CalendarRequest(formattedDate1, formattedDate2);
                Log.e("qwqwqw", "" + formattedDate1 + "--" + formattedDate2);


            }
        });

        CalendarRequest();

        return view;
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();

    }

    public long milliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    private String getDate(String mDate) {

        DateTimeFormatter inputFormatter = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
            LocalDate date = LocalDate.parse(mDate, inputFormatter);
            String appDate = outputFormatter.format(date);

            return appDate;
        } else {
            return mDate;
        }


    }

    private void CalendarRequest() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "booking-details",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {
                        compactCalendarView.removeAllEvents();
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                jsonArrayJSONArray = jsonArray.getJSONObject(1).getJSONArray("bookings");

                                for (int i = 0; i < jsonArrayJSONArray.length(); i++) {
                                    JSONObject object = jsonArrayJSONArray.getJSONObject(i);

                                    int color = Color.parseColor("#5b8bdf");
                                    long timestamp = milliseconds(getDate(object.getString("checkin_date")));
                                    Log.d("miliSecsDate", " = " + timestamp);

                                    Event event = new Event(color, timestamp, object.get("checkin_date"));
                                    compactCalendarView.addEvent(event);

                                }


                            } else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(getActivity(), LoginScreen.class));
                            getActivity().finishAffinity();

                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                //  params.put("email", mEmail);
                // params.put("first_name", mName);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }

    private void CalendarRequest(String start, String end) {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        Log.e("booking", UserSession.BASEURL + "booking-details?start_date=" + start + "&end_date=" + end);
        Log.e("Token", mUserSession.getAPIToken());
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL
                + "booking-details?start_date=" + start + "&end_date=" + end,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(NetworkResponse response) {
                        compactCalendarView.removeAllEvents();
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                jsonArrayJSONArray = jsonArray.getJSONObject(1).getJSONArray("bookings");

                                for (int i = 0; i < jsonArrayJSONArray.length(); i++) {
                                    JSONObject object = jsonArrayJSONArray.getJSONObject(i);

                                    int color = Color.parseColor("#5b8bdf");
                                    long timestamp = milliseconds(getDate(object.getString("checkin_date")));
                                    Log.d("miliSecsDate", " = " + timestamp);

                                    Event event = new Event(color, timestamp, object.get("checkin_date"));
                                    compactCalendarView.addEvent(event);

                                }


                            } else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error.networkResponse.statusCode == 401) {
                            mUserSession.logout();
                            startActivity(new Intent(getActivity(), LoginScreen.class));
                            getActivity().finishAffinity();
                        }
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), R.string.server_error, Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), R.string.bad_netword_connection, Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("owner_id", mUserSession.getUserID());
                //  params.put("email", mEmail);
                // params.put("first_name", mName);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


}