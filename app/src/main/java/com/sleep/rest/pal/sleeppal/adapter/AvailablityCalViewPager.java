package com.sleep.rest.pal.sleeppal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.sleep.pal.sleeppal.R;
import com.sleep.rest.pal.sleeppal.model.UnitListModels;

import java.util.ArrayList;

public class AvailablityCalViewPager extends PagerAdapter {

    private Context mContext;
    private ArrayList<UnitListModels> mUnitListModels;
    private final OnItemClickListener listener;
    public AvailablityCalViewPager(Context context, ArrayList<UnitListModels> eventsImageArray, OnItemClickListener listener) {
        this.mContext = context;
        this.mUnitListModels = eventsImageArray;
        this.listener = listener;

    }
    public interface OnItemClickListener {
        void onItemClick(int item);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.view_pager_event_tickets, container, false);

        TextView mName = view.findViewById(R.id.mName);
        TextView mGuest = view.findViewById(R.id.mGuest);
        TextView mNight = view.findViewById(R.id.mNight);
        TextView mRef = view.findViewById(R.id.mRef);
        TextView mRefID = view.findViewById(R.id.mRefID);
        TextView mRoomName = view.findViewById(R.id.mRoomName);
        TextView mPrice = view.findViewById(R.id.mPrice);
        TextView mTotalPrice = view.findViewById(R.id.mTotalPrice);
        TextView mCHeckin = view.findViewById(R.id.mCHeckin);
        TextView mCHeckOut = view.findViewById(R.id.mCHeckOut);
        ImageView closest = view.findViewById(R.id.closest);

        mName.setText(mUnitListModels.get(position).getName());
        mGuest.setText(mUnitListModels.get(position).getGuest());
        mNight.setText(mUnitListModels.get(position).getNight());
        mRef.setText(mUnitListModels.get(position).getReferral());
        mRefID.setText(mUnitListModels.get(position).getOrderID());
        mRoomName.setText(mUnitListModels.get(position).getRoomName());
        mPrice.setText(mUnitListModels.get(position).getPrice());
        mTotalPrice.setText(mUnitListModels.get(position).getTotal());
        mCHeckin.setText(mUnitListModels.get(position).getCheckIn());
        mCHeckOut.setText(mUnitListModels.get(position).getCheckOut());
        closest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position);
            }
        });


        ((ViewPager) container).addView(view, 0);

        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public int getCount() {
        return mUnitListModels.size();
    }

}