package com.sleep.rest.pal.sleeppal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.sleep.pal.sleeppal.R;

import com.sleep.rest.pal.sleeppal.utils.UserSession;

public class SplashScreen extends AppCompatActivity {


    Handler handler;
    private UserSession mUserSession;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mUserSession = new UserSession(SplashScreen.this);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);


        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(mUserSession.isLoggedIn()){
                    Intent intent = new Intent(SplashScreen.this, MainScreen.class);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = new Intent(SplashScreen.this, LoginScreen.class);
                    startActivity(intent);
                    finish();
                }
            }
        },3000);


    }


}